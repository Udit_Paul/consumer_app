package com.skyinnolabs.consumerapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import org.json.JSONObject;

public class ThankYouActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_thank_you);
        Thread background = new Thread() {
            public void run() {
                try {
/*                    JSONObject object = new JSONObject();
                    object.put("type","transactionCompleted");
                    object.put("store",SigncatchPref.getStoreId());
                    int res = -1;
                    if(SIGNCATCH.mSocket != null && SIGNCATCH.mSocket.connected())
                        res = SIGNCATCH.attemptSend(object);
                    Log.e("Response for res",res+" check");*/
                    // Thread will sleep for 5 seconds
                    sleep(5*1000);

                    // After 5 seconds redirect to another intent
                    startActivity(new Intent(ThankYouActivity.this, MainActivity.class));

                    //Remove activity
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        // start thread
        background.start();
    }
}
