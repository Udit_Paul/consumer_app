package com.skyinnolabs.consumerapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import io.socket.emitter.Emitter;

/**
 * Created by Udit Paul-SignCatch on 7/5/2018.
 */

public class SmartBharatQrCodeFragment extends Fragment implements OnAsycTaskCompletedListener{
    private ImageView ivQrCode;
    private Button btnCheckStatus;
    private TextView qrPaymentLabel;
    private RelativeLayout relProgress, dialogAddNoteClose;
    private String TID = "", MID = "", Image_url = "", txId = "", trId = "", response = "";
    private OrderInfo details;
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View row = inflater.inflate(R.layout.dialog_qr_code, container, false);
        row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        Log.e("Inside ","Bharat Qr");
        initializeViews(row);
        return row;
    }

    // initializing views
    private void initializeViews(View row) {
/*        dialog = getDialog();
        dialog.setCanceledOnTouchOutside(false);*/

        btnCheckStatus = (Button) row.findViewById(R.id.btnCheckStatus);
        btnCheckStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBharatQrCodeDialog();
            }
        });

        qrPaymentLabel = (TextView) row.findViewById(R.id.qrPaymentLabel);

        dialogAddNoteClose = (RelativeLayout) row.findViewById(R.id.dialogAddNoteClose);
        dialogAddNoteClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject object = null;
                try {
                    object = new JSONObject();
                    object.put("type","transactionCancelled");
                    object.put("store",SigncatchPref.getStoreId());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
/*                int res = -1;
                if(SIGNCATCH.mSocket != null && SIGNCATCH.mSocket.connected())
                    res = SIGNCATCH.attemptSend(object);*/
                removeDialog();
            }
        });

        ivQrCode = (ImageView) row.findViewById(R.id.ivQrCode);

        relProgress = (RelativeLayout) row.findViewById(R.id.relProgress);

        Bundle bundle = getArguments();
        details = (OrderInfo) bundle.getSerializable("orderInfo");
        response = bundle.getString("response", response);
        if(SigncatchPref.getBharatQrCredentials()!=null &&
                !SigncatchPref.getBharatQrCredentials().equalsIgnoreCase("")) {
            try {
                JSONObject object = new JSONObject(SigncatchPref.getBharatQrCredentials());
                if (object.has("tid"))
                    TID = object.getString("tid");
                if (object.has("mid"))
                    MID = object.getString("mid");

                JSONObject object1 = new JSONObject(SigncatchPref.getQrResponse());
                if (object1.has("qr_url"))
                    Image_url = object1.getString("qr_url");
                if (object1.has("txnId"))
                    txId = object1.getString("txnId");
                if (object1.has("trId"))
                    trId = object1.getString("trId");

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        Glide.with(this)
                .load(Image_url) // image url
                .apply(new RequestOptions()
                        .placeholder(R.drawable.pos_logo_inside_circle) // any placeholder to load at start
                .error(R.drawable.image_error)  // any image in case of error
                .centerCrop()
                .fitCenter())
                .into(ivQrCode);

        Log.e("Inside 2 ","Bharat Qr");

/*        SIGNCATCH.setEventBharatQrPaymentListener(checkNAddEdc);*/
        qrPaymentLabel.setText("Total Payable Amount : "+details.getRemaining_amount());
    }

    private void removeDialog() {
        if(getActivity() != null) {
            ((PaymentDetailActivity) getActivity()).removeFragment("Bharat Qr");
            ((PaymentDetailActivity)getActivity()).linMain.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
/*        SIGNCATCH.offEventBharatQrPaymentListener(checkNAddEdc);*/
    }

    private void callBharatQrCodeDialog() {
        if(NetworkManager.checkNetwork(getActivity()))
        {
            relProgress.setVisibility(View.VISIBLE);
//                progressView.setVisibility(View.VISIBLE);
//            button.setVisibility(View.GONE);
                Map<String, String> map = new HashMap<>();
                map.put("TID", TID);
                map.put("MID", MID);
                map.put("txnId", txId);
                map.put("txId", txId);
                map.put("trId", trId);
                map.put("order_no", details.getOrder_id());
                map.put("amount", UIToolBox.roundTwoDecimals(SigncatchPref.getQrPayment())+"");
                new MyNetworkAsyncTask(getActivity(), getInventoryUrl(), 20,
                        map, SmartBharatQrCodeFragment.this);
        } else {
            UIToolBox.setAlert(getActivity(),"No network found, connect to internet and try again.");
        }
    }

/*    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        Window window = dialog.getWindow();
        if (window != null) {
            window.requestFeature(Window.FEATURE_NO_TITLE);
        }
        return dialog;
    }*/

    // Connect to EDC listener
/*    public Emitter.Listener checkNAddEdc = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SmartBharatQrCodeFragment.this != null && SmartBharatQrCodeFragment.this.isVisible()) {
                            try {
                                System.out.println(args[0].toString());
                                JSONObject jsonObject = new JSONObject(args[0].toString());
                                if (jsonObject.has("order_id") && jsonObject.has("amount") && jsonObject.has("reference_id")) {

                                    if (jsonObject.getString("order_id").equalsIgnoreCase(details.getOrder_id())) {
                                        onCheckoutSuccess(jsonObject.getString("reference_id"), jsonObject.getString("amount"));
                                    }
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            }
        }
    };*/

    private String getInventoryUrl()
    {
        String url ="/payment/check-status?access-token="+SigncatchPref.getAccessToken()+"&type="+SigncatchPref.getLoginType();
        return url;
    }

    @Override
    public void onTaskCompleted(String response, int RequestCode) {
        if(response!=null)
            if(RequestCode == 222) {
                try {
                    relProgress.setVisibility(View.GONE);
                    if(response!=null && response.contains("Successfully Done")) {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.has("payment_details"))
                            details.setPayments(jsonObject.getString("payment_details"));
                        if (jsonObject.has("bharat_qr_details"))
                            details.setBharatQrDetails(jsonObject.getString("bharat_qr_details"));
                        if (jsonObject.has("remaining_amount")) {
                            details.setRemaining_amount(jsonObject.getString("remaining_amount"));
                        }
                        if (jsonObject.has("refund_amount"))
                            details.setRefund(jsonObject.getString("refund_amount"));

                    JSONObject object = new JSONObject();
                    object.put("type","transactionCompleted");
                    object.put("store",SigncatchPref.getStoreId());
                    if(SIGNCATCH.mSocket != null && SIGNCATCH.mSocket.connected())
                        SIGNCATCH.attemptSend(object);

/*                        double remainingAmount = Double.parseDouble(details.getRemaining_amount());

                            if (remainingAmount == 0 && getActivity()!=null) {
*//*                                Intent intent = new Intent(getActivity(), OrderSuccessActivity.class);
                                intent.putExtra("detail",details);
                                startActivity(intent);
                                getActivity().finish();*//*
                            }*/
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                removeDialog();
//                dialog.dismiss();
            } else {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String ref_no = "", txn_amt = "";
                    if (jsonObject.has("responseObject") && !jsonObject.isNull("responseObject")) {
                        JSONObject jsonObject1 = new JSONObject(jsonObject.getString("responseObject"));
                        if (jsonObject1.has("ref_no") && !jsonObject1.isNull("ref_no"))
                            ref_no = jsonObject1.getString("ref_no");
                        if (jsonObject1.has("txn_amount") && !jsonObject1.isNull("txn_amount"))
                            txn_amt = jsonObject1.getString("txn_amount");
                            onCheckoutSuccess(ref_no, Double.parseDouble(txn_amt)+"");
                    } else if (jsonObject.has("message") && !jsonObject.isNull("message")) {
                        relProgress.setVisibility(View.GONE);
                        UIToolBox.setAlert(getContext(), jsonObject.getString("message"));
                    }
                } catch (JSONException e) {
                    relProgress.setVisibility(View.GONE);
                    e.printStackTrace();
                }
                Log.e("Successful message", "Completed");
            }
    }

    @Override
    public void onTaskFailed(String response, int RequestCode, int statusCode) {
        if(this!=null && this.isVisible()) {
            relProgress.setVisibility(View.GONE);
            UIToolBox.errorAlert(getActivity(), response, statusCode);
        }
    }

    private void onCheckoutSuccess(String reference_no, String amount) {
        if(getContext()!=null) {
            if (NetworkManager.checkNetwork(getContext())) {
                Map<String, String> map = new HashMap<>();
                map.put("order_id", details.getId());
                map.put("transaction_status", "3");
                map.put("payment_type", "14");
                map.put("amount", amount);
                map.put("payment_detail", reference_no);
                map.put("reference_id", reference_no);

                    map.put("override_access_info[" + 0 + "][manager_id]", SigncatchPref.getMerchantId());
                    map.put("override_access_info[" + 0 + "][access_code]", "");
                    if (details.getInstore().equals("1"))
                        map.put("override_access_info[" + 0 + "][override_type]", "retail_order");
                    else if (details.getDinein().equals("1"))
                        map.put("override_access_info[" + 0 + "][override_type]", "dinein");
                    else if (details.getPickup_detail().equals("1"))
                        map.put("override_access_info[" + 0 + "][override_type]", "pickup_order");
                    else if (details.getDelivery_detail().equals("1")) {
                        if (details.getChannel() == 3)
                            map.put("override_access_info[" + 0 + "][override_type]", "whole_sale_order");
                        else if (details.getChannel() == 2)
                            map.put("override_access_info[" + 0 + "][override_type]", "online_order");
                        else
                            map.put("override_access_info[" + 0 + "][override_type]", "instore_order");
                    } else
                        map.put("override_access_info[" + 0 + "][override_type]", "");
                    map.put("override_access_info[" + 0 + "][action_type]", "payment");
                    map.put("override_access_info[" + 0 + "][product_id]", "0");
                    map.put("override_access_info[" + 0 + "][old_value]", "");
                    map.put("override_access_info[" + 0 + "][new_value]", UIToolBox.roundTwoDecimals(details.getTranscationAmount()));
                new MyNetworkAsyncTask(getActivity(), "/order/on-checkout-success?access-token=" + SigncatchPref.getAccessToken() + "&type=" + SigncatchPref.getLoginType(), 345, map, this);
            } else
                UIToolBox.setAlert(getContext(), "No network found, connect to internet and try again.");
        }
    }

}
