package com.skyinnolabs.consumerapp;

/**
 * Created by Dharmpal Verma on 2/10/2017.
 */

// Listener for completion of async tasks in all APIs
public interface OnAsycTaskCompletedListener {
    void onTaskCompleted(String response, int RequestCode);
    void onTaskFailed(String response, int RequestCode, int statusCode);
}
