package com.skyinnolabs.consumerapp;

/**
 * Created by Dharmpal Verma on 7/22/2017.
 */

// Listener for tasks to be performed on communication with web-socket in MainActivity
public interface WebSocketCommunicationLisntner {
    public void onConnected(Object... args);
    public void onDisconnected(Object... args);
}
