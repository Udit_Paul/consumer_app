package com.skyinnolabs.consumerapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by Dharmpal on 4/22/2016.
 */
public class UIToolBox {
   static  boolean isShow;
    public static void TOAST_MSG(Context context,String text)
     {
         Toast   toast  = Toast.makeText(context,text,Toast.LENGTH_SHORT);
         toast.setGravity(Gravity.CENTER,0,0);
         toast.show();
     }

    public static String getStringValue(JSONObject object,String key,String def) throws JSONException {
        return object.has(key) && !object.isNull(key) && !object.getString(key).equals("false")? object.getString(key):def;
    }

    public static String getStringValue(JSONObject object,String key) throws JSONException {
        return object.has(key) && !object.isNull(key) && !object.getString(key).equals("false")? object.getString(key):"";
    }
    public  static double getDoubleValue(JSONObject object,String key) throws JSONException {
        return object.has(key) && !object.isNull(key)? object.getDouble(key):0d;
    }

    public  static boolean getBooleanValue(JSONObject object,String key) throws JSONException {
        return object.has(key) && !object.isNull(key)? object.getBoolean(key):false;
    }
    public  static int getIntValue(JSONObject object,String key) throws JSONException {
        return object.has(key) && !object.isNull(key)? object.getInt(key):0;
    }

    public static boolean isJsonArray(JSONObject js,String str) throws JSONException {
        boolean isJsonArray=false;
        if(getStringValue(js,str,"").equalsIgnoreCase(""))
            isJsonArray= false;
        Object json = new JSONTokener(js.getString(str)).nextValue();
        if (json instanceof JSONArray)
            isJsonArray= true;
        else
            isJsonArray=false;
        if(SIGNCATCH.IS_DEBUGGING)
            Log.e("JSON ERROR","\nIsJsonArray: "+isJsonArray+"\n Key:"+str+"\nValue: "+js.getString(str)+"\n_____________________");
        return  isJsonArray;
    }


    public static boolean isJsonObject(JSONObject js,String str) throws JSONException {
        boolean isJsonObject=false;
        if(getStringValue(js,str,"").equalsIgnoreCase(""))
            isJsonObject=false;
        Object json = new JSONTokener(js.getString(str)).nextValue();
        if (json instanceof JSONObject)
            isJsonObject=true;
        else
            isJsonObject=false;
        if(SIGNCATCH.IS_DEBUGGING)
            Log.e("JSON ERROR","\nIsJsonObject: "+isJsonObject+"\n Key:"+str+"\nValue: "+js.getString(str)+"\n_____________________");
        return isJsonObject;
    }


    public static String  getErrorAlertMsg(String response,int status) {
        String msg ="";
        if(response!=null && response.startsWith("[") && response.contains("message")) {
            try {
                JSONArray array = new JSONArray(response);
                for(int i=0;i<array.length();i++)
                {
                    msg =msg+array.getJSONObject(i).getString("message")+"\n";
                }
            } catch (JSONException e) {
                msg ="Validation error.";
                e.printStackTrace();
            }
        }
        else if (response!=null && response.startsWith("{") && response.contains("message"))
        {
            try {
                JSONObject jsonObject = new JSONObject(response);
                msg = jsonObject.getString("message");
            } catch (JSONException e) {
                msg ="Not found.";
                e.printStackTrace();
            }
        }
        else if (response!=null && response.startsWith("{") && response.contains("error"))
        {
            try {
                JSONObject jsonObject = new JSONObject(response);

                if(jsonObject.getString("error").startsWith("["))
                {
                    try {
                        JSONArray array = jsonObject.getJSONArray("error");
                        for(int i=0;i<array.length();i++)
                        {
                            msg =msg+array.getString(i)+"\n";
                        }
                    } catch (JSONException e) {
                        msg ="Validation error.";
                        e.printStackTrace();
                    }
                }
                else
                    msg = jsonObject.getString("error");
            } catch (JSONException e) {
                msg ="Not found.";
                e.printStackTrace();
            }
        }
        else if(HttpStatusCodes.getCodesMap()!=null && HttpStatusCodes.getCodesMap().get(status)!=null)
        {
            msg = HttpStatusCodes.getCodesMap().get(status);
        }
        else
            msg = "Something went wrong, try again.";

        return msg;
    }

    public static void errorAlert(Context context,String response,int status) {
         setAlert(context,getErrorAlertMsg(response,status));
    }

    public static void Custum_Toast_success(Context context,String text)
    {
         LayoutInflater  inflater =LayoutInflater.from(context);
         Toast   toast  = new Toast(context);
         View view = inflater.inflate(R.layout.custum_toast_layout,null);
         TextView textView = (TextView) view.findViewById(R.id.tvToastText);
         textView.setText(text);
         toast.setView(view);
         toast.setDuration(Toast.LENGTH_LONG);
         toast.setGravity(Gravity.CENTER,0,0);
         toast.show();
    }

    public static void Custum_Toast_fail(Context context,String text)
    {
        LayoutInflater  inflater =LayoutInflater.from(context);
        Toast   toast  = new Toast(context);
        View view = inflater.inflate(R.layout.custum_toast_layout,null);
        TextView textView = (TextView) view.findViewById(R.id.tvToastText);
        ImageView ivToast = (ImageView) view.findViewById(R.id.ivToast);
        ivToast.setImageResource(R.drawable.error);
        textView.setTextColor(Color.parseColor("#f39f33"));
        textView.setText(text);
        toast.setView(view);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER,0,0);
        toast.show();
    }
    public static String roundTwoDecimals(double d) {
/*        Log.e("Actual", d+"");
        Log.e("Old method", String.format("%.2f",d));*/
/*        DecimalFormat df = new DecimalFormat("####0.00");
        Log.e("New method", df.format(d));*/
        return String.format("%.2f",d);
    }

    public static String roundTwoDecimals(String str) {
        Double d = Double.parseDouble(str);
        return String.format("%.2f",d);
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

    public static void setAlert(Context context,String msg)
    {
        try {
            final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
// ...Irrelevant code for customizing the buttons and title
            LayoutInflater inflater = LayoutInflater.from(context);
            View dialogView = inflater.inflate(R.layout.alert_dialog_layout, null);
            TextView tvErrorMsg = (TextView) dialogView.findViewById(R.id.tvErrorMsg);
            ImageView ivClose = (ImageView) dialogView.findViewById(R.id.ivClose);
            tvErrorMsg.setText(msg);
            dialogBuilder.setView(dialogView);
            final AlertDialog alertDialog = dialogBuilder.create();

            ivClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void setSuccessAlert(Context context,String msg)
    {
        try {
            if(context!=null) {
                final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
    // ...Irrelevant code for customizing the buttons and title
                LayoutInflater inflater = LayoutInflater.from(context);
                View dialogView = inflater.inflate(R.layout.success_alert_dialog_layout, null);
                TextView tvErrorMsg = (TextView) dialogView.findViewById(R.id.tvSuccessMsg);
                ImageView ivClose = (ImageView) dialogView.findViewById(R.id.ivClose);
                tvErrorMsg.setText(msg);
                dialogBuilder.setView(dialogView);
                final AlertDialog alertDialog = dialogBuilder.create();

                ivClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });
                alertDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
