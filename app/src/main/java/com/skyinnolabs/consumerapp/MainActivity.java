package com.skyinnolabs.consumerapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.ResultPoint;
import com.google.zxing.client.android.BeepManager;
import com.google.zxing.integration.android.IntentIntegrator;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.CompoundBarcodeView;
import com.journeyapps.barcodescanner.camera.CameraSettings;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import io.socket.emitter.Emitter;

public class MainActivity extends Activity implements OnAsycTaskCompletedListener {
    private CompoundBarcodeView barcodeView;
    View relLoading;
    String str = "", name = "", userInfo = "";
    TextView tvContinue, tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9, tv10;
    EditText etConsumerName, etConsumerMail, etMobileNo;
    LinearLayout linName, linEmail;

    private static final int CAMERA_PERMISSION_REQUEST = 68 ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_mains);
        barcodeView = findViewById(R.id.barcode_scanner);
        relLoading = findViewById(R.id.relLoading);
        tvContinue = findViewById(R.id.tvContinue);
        etConsumerMail = findViewById(R.id.etConsumerMail);
        etConsumerName = findViewById(R.id.etConsumerName);
        etMobileNo = findViewById(R.id.etMobileNo);

        linName = findViewById(R.id.linName);
        linEmail = findViewById(R.id.linEmail);

        SIGNCATCH.setDataToDeviceLinstener(dataToDeviceLinstener);

        checkPermission();

        tvContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etConsumerName.getText().toString().trim().equalsIgnoreCase(""))
                    etConsumerName.setError("It's a mandatory field");
                else {
                    try {
                        SIGNCATCH.HIDE_SOFTKEYBOARD(MainActivity.this);
                        JSONObject object = new JSONObject();
                        JSONObject objOrder = new JSONObject(userInfo);
                        object.put("user", objOrder);
                        object.put("number", etMobileNo);
                        object.put("name", etConsumerName.getText().toString().trim());
                        object.put("mail", etConsumerMail.getText().toString().trim());
                        object.put("store",SigncatchPref.getStoreId());
                        object.put("type","customer");
                        object.put("userFound","0");
                        String uName = etConsumerName.getText().toString().trim();
                        String[] name = uName.split(" ");
                        if (name.length > 1) {
                            String fName = uName.replace(name[name.length - 1], "");
                            object.put("first_name", fName);
                            object.put("last_name",name[name.length - 1]);
                        } else {
                            object.put("first_name", uName);
                            object.put("last_name","");
                        }
                        SIGNCATCH.setDataToDeviceLinstener(dataToDeviceLinstener);
                        if(SIGNCATCH.mSocket != null && SIGNCATCH.mSocket.connected()) {
                            SIGNCATCH.attemptSend(object);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        etMobileNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()>0) {
                    switch(s.length()) {
                        case 1:
                            tv1.setAlpha(1f);
                            tv1.setText(s.toString().charAt(0));
                            tv2.setText("0");
                            tv2.setAlpha(0.4f);
                            break;
                        case 2:
                            tv2.setAlpha(1f);
                            tv2.setText(s.toString().charAt(1));
                            tv3.setText("0");
                            tv3.setAlpha(0.4f);
                            break;
                        case 3:
                            tv3.setAlpha(1f);
                            tv3.setText(s.toString().charAt(2));
                            tv4.setText("0");
                            tv4.setAlpha(0.4f);
                            break;
                        case 4:
                            tv4.setAlpha(1f);
                            tv4.setText(s.toString().charAt(3));
                            tv5.setText("0");
                            tv5.setAlpha(0.4f);
                            break;
                        case 5:
                            tv5.setAlpha(1f);
                            tv5.setText(s.toString().charAt(4));
                            tv6.setText("0");
                            tv6.setAlpha(0.4f);
                            break;
                        case 6:
                            tv6.setAlpha(1f);
                            tv6.setText(s.toString().charAt(5));
                            tv7.setText("0");
                            tv7.setAlpha(0.4f);
                            break;
                        case 7:
                            tv7.setAlpha(1f);
                            tv7.setText(s.toString().charAt(6));
                            tv8.setText("0");
                            tv8.setAlpha(0.4f);
                            break;
                        case 8:
                            tv8.setAlpha(1f);
                            tv8.setText(s.toString().charAt(7));
                            tv9.setText("0");
                            tv9.setAlpha(0.4f);
                            break;
                        case 9:
                            tv9.setAlpha(1f);
                            tv9.setText(s.toString().charAt(8));
                            tv10.setText("0");
                            tv10.setAlpha(0.4f);
                            break;
                        case 10:
                            tv10.setAlpha(1f);
                            tv10.setText(s.toString().charAt(9));
                            findCustomer();
                            break;
                    }
                }
            }
        });

    }

    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(MainActivity.this,
                android.Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    android.Manifest.permission.CAMERA)) {
                new AlertDialog.Builder(this)
                        .setIcon(R.drawable.ic_info)
                        .setMessage("Camera permission is necessary for scanning barcode code")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.CAMERA}, CAMERA_PERMISSION_REQUEST);
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })
                        .show();
            } else {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{android.Manifest.permission.CAMERA},
                        CAMERA_PERMISSION_REQUEST);
            }
        } else {

            barcodeView.setStatusText(null);
            barcodeView.decodeContinuous(callback);

            if (Camera.getNumberOfCameras() > 1) {
                CameraSettings settings = barcodeView.getBarcodeView().getCameraSettings();
                if (barcodeView.getBarcodeView().isPreviewActive()) {
                    barcodeView.pause();
                }
                //swap the id of the camera to be used
                /*            if (settings.getRequestedCameraId() == Camera.CameraInfo.CAMERA_FACING_BACK) {*/
                settings.setRequestedCameraId(Camera.CameraInfo.CAMERA_FACING_FRONT);
/*            } else {
                settings.setRequestedCameraId(Camera.CameraInfo.CAMERA_FACING_BACK);
            }*/
                barcodeView.getBarcodeView().setCameraSettings(settings);

                barcodeView.resume();

            } else {
                barcodeView.setVisibility(View.GONE);
            }

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults!=null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

        }
        else if(grantResults!=null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED)
        {
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,android.Manifest.permission.CAMERA)) {

                new AlertDialog.Builder(this)
                        .setIcon(R.drawable.ic_info)
                        .setMessage("Camera permission is necessary for scanning barcode code")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.CAMERA}, CAMERA_PERMISSION_REQUEST);
                            }
                        })
                        .setNegativeButton("No",null)
                        .show();
            }
            else
            {
                new AlertDialog.Builder(this)
                        .setIcon(R.drawable.ic_info)
                        /*.setTitle(R.string.rate_title)*/
                        .setMessage("This feature would like to access your device's camera. Grant permission.")
                        .setPositiveButton("Grant Permission", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package", getPackageName(), null);
                                intent.setData(uri);
                                startActivityForResult(intent, 123);
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })
                        .show();
            }
        }
    }

    private BarcodeCallback callback = new BarcodeCallback() {
        @Override
        public void barcodeResult(BarcodeResult result) {

        }
        @Override
        public void possibleResultPoints(List<ResultPoint> resultPoints) {

        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        barcodeView.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        barcodeView.pause();
    }

    @Override
    protected void onDestroy() {
        SIGNCATCH.offDataToDeviceLinstener(dataToDeviceLinstener);
        super.onDestroy();
    }

    private void findCustomer() {
        if (NetworkManager.checkNetwork(MainActivity.this)) {
            String url = "/buyer/find-user-details/" + "+91/" + str + "?access-token=" + SigncatchPref.getAccessToken() + "&type="+SigncatchPref.getLoginType();
            new MyNetworkAsyncTask(MainActivity.this, url, 40, this);
        } else
            UIToolBox.Custum_Toast_fail(MainActivity.this, "No network found, connect to internet and try again.");
    }

    @Override
    public void onBackPressed() {
        SIGNCATCH.setDataToDeviceLinstener(dataToDeviceLinstener);
        try {
            JSONObject object = new JSONObject();
            object.put("store",SigncatchPref.getStoreId());
            object.put("type","cancel");
            if(SIGNCATCH.mSocket != null && SIGNCATCH.mSocket.connected())
                SIGNCATCH.attemptSend(object);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        super.onBackPressed();
    }

    @Override
    public void onTaskCompleted(String response, int RequestCode) {

        try {
            JSONObject jsonObject = new JSONObject(response);
            if(jsonObject.has("user")) {
                JSONObject jsonObject1 = jsonObject.getJSONObject("user");
                name = UIToolBox.getStringValue(jsonObject1,"first_name","") +" "+ UIToolBox.getStringValue(jsonObject1,"last_name","");
                if(!name.trim().equalsIgnoreCase(""))
                    this.name = name;
                if(!UIToolBox.getStringValue(jsonObject1,"first_name","").equalsIgnoreCase("")) {
                    JSONObject object = new JSONObject();
                    object.put("user", jsonObject.getJSONObject("user"));
                    object.put("store",SigncatchPref.getStoreId());
                    object.put("type","customer");
                    object.put("userFound","1");
                    if(SIGNCATCH.mSocket != null && SIGNCATCH.mSocket.connected()) {
                        SIGNCATCH.attemptSend(object);
                        SIGNCATCH.setDataToDeviceLinstener(dataToDeviceLinstener);
                    }
                } else {
                    linName.setVisibility(View.VISIBLE);
                    linEmail.setVisibility(View.VISIBLE);
                    tvContinue.setVisibility(View.VISIBLE);
                    userInfo = jsonObject.getString("user");
/*                    Intent itn = new Intent(MainActivity.this, CustomerInfoActivity.class);
                    itn.putExtra("userInfo",jsonObject.getString("user"));
                    itn.putExtra("userNo",str);
                    startActivity(itn);
                    finish();*/
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskFailed(String response, int RequestCode, int statusCode) {
        if(this!=null && !this.isDestroyed()) {
            UIToolBox.errorAlert(MainActivity.this, response, statusCode);
        }
    }

    public Emitter.Listener dataToDeviceLinstener = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (args[0] != null) {
                        if (args[0].toString().contains("type")) {
                            try {
                                Log.e("Response is",args[0]+"");
                                JSONObject jsonObject = new JSONObject(args[0].toString());
                                if(jsonObject.getString("type").equalsIgnoreCase("transactionStarted")) {
                                    Toast.makeText(MainActivity.this, "Begin Transaction!", Toast.LENGTH_SHORT).show();
                                    if(jsonObject.has("order") && !jsonObject.isNull("order") && !jsonObject.getString("order").equalsIgnoreCase("")) {
                                        Intent intent = new Intent(MainActivity.this,PaymentDetailActivity.class);
                                        intent.putExtra("orderInfo",jsonObject.getString("order"));
                                        startActivity(intent);
                                        finish();
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            });
        }
    };

}
