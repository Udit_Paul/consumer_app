package com.skyinnolabs.consumerapp;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LoginActivity extends Activity implements OnAsycTaskCompletedListener{
    EditText username;
    EditText password;
    TextView tvDeviceId;
    TextView tvVersionId;
    Button login;
    RelativeLayout progressView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);

        username = findViewById(R.id.fragLoginEditTextUserName);
        password = findViewById(R.id.fragLoginEditTextPassword);
        tvDeviceId = findViewById(R.id.tvDeviceId);
        tvVersionId = findViewById(R.id.tvVersionId);
        login = findViewById(R.id.fragLoginButtonLogin);
        progressView = findViewById(R.id.viewProgress);

        tvVersionId.setText("v"+BuildConfig.VERSION_NAME+"-"+this.getString(R.string.app_type));
        tvDeviceId.setText(SigncatchPref.getDeviceName());

        username.requestFocus();
        InputMethodManager imm = (InputMethodManager)getSystemService(Service.INPUT_METHOD_SERVICE);
        try {
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        } catch (Exception e) {
            Log.e("Error is", "Null pointer exception");
        }

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!username.getText().toString().trim().equalsIgnoreCase("") && !password.getText().toString().equalsIgnoreCase(""))
                {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    if(NetworkManager.checkNetwork(LoginActivity.this))
                    {
                        progressView.setVisibility(View.VISIBLE);
                        Map<String,String> map = new HashMap();
                        map.put("username",username.getText().toString());
                        map.put("password",password.getText().toString());
                        map.put("device_id",SigncatchPref.getDeviceName());
                        map.put("pushy_registration_token","");
                        SigncatchPref.setLoginType(1);
                        map.put("type", "1");
                        new MyNetworkAsyncTask(LoginActivity.this,"/login?device_id="+SigncatchPref.getDeviceName(), 30,map,LoginActivity.this);
                    } else {
                        UIToolBox.setAlert(LoginActivity.this, "No network found, connect to internet and try again.");
                    }
                }
            }
        });

    }

    public void loginResponse(JSONObject object) throws JSONException {

        /*        if(object.has("role_id") && !object.getString("role_id").equalsIgnoreCase("0")) {*/
        if (object.has("merchant_id"))
            SigncatchPref.setMerchantId(object.getString("merchant_id"));
        if (object.has("access_token"))
            SigncatchPref.setAccessToken(object.getString("access_token"));
        if (object.has("store_location")) {
            JSONArray mStoreArr = object.getJSONArray("store_location");
            for (int i = 0; i < mStoreArr.length(); i++) {
                if(mStoreArr.getJSONObject(i).has("id")
                        && !mStoreArr.getJSONObject(i).isNull("id")
                        && !mStoreArr.getJSONObject(i).getString("id").equalsIgnoreCase(""))
                    SigncatchPref.setStoreId(mStoreArr.getJSONObject(i).getString("id"));
                else
                    SigncatchPref.setStoreId("1");
            }
        }

        if(object.has("bharat_qr_credentials"))
            SigncatchPref.setBharatQrCredentials(object.getString("bharat_qr_credentials"));

        if(SIGNCATCH.mSocket!=null && SIGNCATCH.mSocket.connected())
            SIGNCATCH.registerDevice();
        else if(SIGNCATCH.mSocket != null && !SIGNCATCH.mSocket.connected())
        {
            if (SIGNCATCH.mSocket == null || !SIGNCATCH.mSocket.connected())
                ((SIGNCATCH) SIGNCATCH.getContext()).getSocket();
            if(SIGNCATCH.mSocket.connected())
                SIGNCATCH.registerDevice();
        }
        startActivity(new Intent(LoginActivity.this,MainActivity.class));
    }

    @Override
    public void onTaskCompleted(String response, int RequestCode) {
        if(response!=null && response.contains("access_token"))
        {
            try {
                JSONObject object = new JSONObject(response);
                loginResponse(object);
            } catch (JSONException e) {
                e.printStackTrace();
                UIToolBox.setAlert(this, "Unable to login.Please try again.");
                progressView.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onTaskFailed(String response, int RequestCode, int statusCode) {
        if(this!=null && !this.isDestroyed()) {
            if(progressView!=null)
                progressView.setVisibility(View.GONE);
            UIToolBox.errorAlert(this, response, statusCode);
        }
    }
}
