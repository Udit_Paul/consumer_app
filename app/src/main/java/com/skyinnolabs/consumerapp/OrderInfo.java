package com.skyinnolabs.consumerapp;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dharmpal Verma on 2/9/2017.
 */

public class OrderInfo implements Serializable {
    private String id;
    private String layout_id;
    private String order_id;
    private String amount="0.0";
    private String splitPayment="0";
    private String company_name;
    private String payment_method;
    private String payment_details;
    private String update_date;
    private String created_date;
    private int transaction_status;
    private CustomerInfo userInfo;
    private String customer_name="";
    private String discount_amount="0";
    private String completed="0";
    private String order_note="";
    private String price;
    private int channel;
    private String discount_percentage;
    private String store_location;
    private String remaining_amount="0.0";
    private  String bill_receipt_info="";
    private String customer_phone_no="";
    private String isd_code="";
    private String response="";
    private String tax_amount;
    private String pos;
    private String smart_checkout;
    private String marketplace;
    private String delivery_detail;
    private String pickup_detail;
    private String instore;
    private String dinein;
    private String customer_email="";
    private String cover;
    private String merchant_location_id;
    private String special_discount="0";
    private String product_discount="0";
    private String special_discount_percentage;
    private String order_discount_percentage;
    private String manager_name;
    private String pickup_id;
    private String tender;
    private String balance;
    private String refund;
    private String change;
    private String cash_received;
    private String cash_returned;
    private String open;
    private String promocode_amount = "0";
    private String promocode_id = "0";
    private String tax_id;
    private double transcationAmount;
    private boolean isSplit = false;
    private String refunded_order_id;
    private String delivery_date = "";
    private String shipping_status;
    private String packer_person_id;
    private String delivery_person_id;
    private double subsidized_amount=0d;
    private double actual_amount=0d;

    public String getOpen() {
        return open;
    }

    public boolean isSplit() {
        return isSplit;
    }

    public void setSplit(boolean split) {
        isSplit = split;
    }

    public void setRefund(String refund) {
        this.refund = refund;
    }

    public double getTranscationAmount() {
/*            transcationAmount = Math.round(transcationAmount);*/
        return transcationAmount;
    }

    public String getTax_id() {
        return tax_id;
    }

    public void setTranscationAmount(double transcationAmount) {
        this.transcationAmount = transcationAmount;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getRefunded_order_id() {
        return refunded_order_id;
    }

    public String getChange() {
        return change;
    }

    public String getTender() {
        return tender;
    }

    public void setChange(String change) {
        this.change = change;
    }

    public String getRefund() {
        return refund;
    }

    public String getBalance() {
        return balance;
    }

    public String getCreated_date() {
        return created_date;
    }

    public String getOrder_id() {
        return order_id;
    }

    public String getLayout_id() {
        return layout_id;
    }

    public String getManager_name() {
        return manager_name;
    }

    public String getShipping_status() {
        return shipping_status;
    }

    public String getPacker_person_id() {
        return packer_person_id;
    }

    public String getDelivery_person_id() {
        return delivery_person_id;
    }

    public String getDelivery_date() {
        return delivery_date;
    }

    public void setDelivery_date(String delivery_date) {
        this.delivery_date = delivery_date;
    }

    public void setRemaining_amount(String remaining_amount) {
        this.remaining_amount = remaining_amount;
    }

    private List<PaymentDetail> paymentList;

    private List<BharatQrDetails> bharatQrDetailsList;

    public String getPickup_id() {
        return pickup_id;
    }

    public String getSpecial_discount() {
        return special_discount;
    }

    public String getSpecial_discount_percentage() {
        return special_discount_percentage;
    }

    public String getOrder_discount_percentage() {
        return order_discount_percentage;
    }

    public String getMerchant_location_id() {
        return merchant_location_id;
    }

    public String getCover() {
        return cover;
    }

    public String getCustomer_email() {
        return customer_email;
    }

    public String getDelivery_detail() {
        return delivery_detail;
    }

    public String getPickup_detail() {
        return pickup_detail;
    }

    public String getInstore() {
        return instore;
    }

    public String getDinein() {
        return dinein;
    }

    public String getPos() {
        return pos;
    }

    public String getSmart_checkout() {
        return smart_checkout;
    }

    public String getMarketplace() {
        return marketplace;
    }

    public String getRemaining_amount() {
/*        double rAmt = Double.parseDouble(remaining_amount);
            rAmt = Math.round(rAmt);*/
        return remaining_amount;
    }

    public String getTax_amount() {
        return tax_amount;
    }

    public String getCustomer_phone_no() {
        return customer_phone_no;
    }

    public String getIsd_code() {
        return isd_code;
    }

    public String getStore_location() {
        return store_location;
    }

    public String getBill_receipt_info() {
        return bill_receipt_info;
    }

    public String getDiscount_percentage() {
        return discount_percentage;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDiscount_amount() {
        return discount_amount;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public String getCompleted() {
        return completed;
    }

    public void setCompleted(String completed) {
        this.completed = completed;
    }

    public int getChannel() {
        return channel;
    }

    public List<PaymentDetail> getPaymentList() {
        return paymentList;
    }

    public List<BharatQrDetails> getBharatQrDetailsList() {
        return bharatQrDetailsList;
    }

    public OrderInfo(String response)
    {
        this.response = response;
        try {
            JSONObject objOrder =new JSONObject(response);
            if(objOrder.has("orderInfo"))
                setOrderInfo(objOrder.getString("orderInfo"));
            if(objOrder.has("merchantInfo"))
                setCompany_name(objOrder.getString("merchantInfo"));
            if(objOrder.has("payments") && !objOrder.isNull("payments"))
              setPayments(objOrder.getString("payments"));
/*            if(objOrder.has("bharat_qr_details") && !objOrder.isNull("bharat_qr_details"))
                setBharatQrDetails(objOrder.getString("bharat_qr_details"));*/
            if(objOrder.has("user"))
                setUserInfo(objOrder.getString("user"));


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setPayments(String response) throws JSONException {
        Log.e("Response 1", response+" not null");
        if(response!=null && !response.equalsIgnoreCase("null") && !response.equalsIgnoreCase("")) {
            JSONArray array = new JSONArray(response);
            Log.e("Response", response+" not null");
            paymentList = new ArrayList<>();
            for (int i = 0; i < array.length(); i++) {
                PaymentDetail paymentDetail = new PaymentDetail(array.getString(i));
                paymentList.add(paymentDetail);
            }
        }
    }

    public void setBharatQrDetails(String response) throws JSONException {
        Log.e("Response 1", response+" not null");
        if(response!=null && !response.equalsIgnoreCase("null") && !response.equalsIgnoreCase("")) {
            JSONArray array = new JSONArray(response);
            Log.e("Response", response+" not null");
            bharatQrDetailsList = new ArrayList<>();
            for (int i = 0; i < array.length(); i++) {
                BharatQrDetails bharatQrDetails = new BharatQrDetails(array.getString(i));
                bharatQrDetailsList.add(bharatQrDetails);
            }
        }
    }

     public void setUserInfo(String  user) {
        userInfo =  new CustomerInfo(user);
    }

    public void setUserInfo(CustomerInfo  user) {
         this.userInfo= user;
    }

    public CustomerInfo getUserInfo() {
        return userInfo;
    }

     public void setOrderInfo(String response)
     {
         try {
             JSONObject object = new JSONObject(response);
              if(object.has("id"))
                  id = object.getString("id");
              if(object.has("amount")) {
                  amount = object.getString("amount");
              }
              if(object.has("payment_method"))
              payment_method = object.getString("payment_method");
             if(object.has("pos"))
                 pos = object.getString("pos");
             if(object.has("marketplace"))
                 marketplace = object.getString("marketplace");
             if(object.has("smart_checkout"))
                 smart_checkout = object.getString("smart_checkout");
              if(object.has("payment_detail"))
                  payment_details = object.getString("payment_detail");
             if(object.has("store_location"))
                 store_location = object.getString("store_location");
             if(object.has("bharat_qr_details") && !object.isNull("bharat_qr_details"))
                 setBharatQrDetails(object.getString("bharat_qr_details"));
             if(object.has("bill_receipt_info") && !object.isNull("bill_receipt_info"))
                 bill_receipt_info = object.getString("bill_receipt_info");
             if(object.has("tax_amount"))
                 tax_amount = object.getString("tax_amount");
             if(object.has("remaining_amount")) {
                 remaining_amount = object.getString("remaining_amount");
                transcationAmount =object.getDouble("remaining_amount");
             }
             if(object.has("actual_amount"))
                 actual_amount = object.getDouble("actual_amount");
              if(object.has("updated_at"))
                  update_date = object.getString("updated_at");
             if(object.has("created_at"))
             created_date=object.getString("created_at");
              if(object.has("transaction_status"))
                 transaction_status = object.getInt("transaction_status");
              if(object.has("customer_name") && !object.isNull("customer_name"))
                customer_name = object.getString("customer_name");
             if(object.has("customer_phone_no") && !object.isNull("customer_phone_no"))
                 customer_phone_no = object.getString("customer_phone_no");
             if(object.has("isd_code") && !object.isNull("isd_code"))
                 isd_code = object.getString("isd_code");
              if(object.has("discount_amount"))
                discount_amount = object.getString("discount_amount");
             if(object.has("price"))
                 price = object.getString("price");
             if(object.has("discount_percentage"))
             discount_percentage = object.getString("discount_percentage");
             refunded_order_id = getStringValue(object,"store_refunded_order_id","0");
             shipping_status = getStringValue(object,"shipping_status","0");
             packer_person_id = getStringValue(object,"packer_person_id","0");
             delivery_person_id = getStringValue(object,"delivery_person_id","0");
             delivery_detail =object.has("delivery_detail")?object.getString("delivery_detail"):"0";
             pickup_detail=object.has("pickup_detail")?object.getString("pickup_detail"):"0";
             instore=object.has("instore")?object.getString("instore"):"0";
             dinein=object.has("dinein")?object.getString("dinein"):"0";
             customer_email = object.has("customer_email") && !object.isNull("customer_email")?object.getString("customer_email"):"";
             cover = object.has("cover")?object.getString("cover"):"1";
             merchant_location_id =getStringValue(object,"merchant_location_id","");
             special_discount_percentage =getStringValue(object,"special_discount_percentage","0");
             special_discount =getStringValue(object,"special_discount","0");
             channel =object.has("channel")?object.getInt("channel"):1;
             Log.e("address:",getStringValue(object,"deliveryDetail",""));
             if(!getStringValue(object,"deliveryDetail","").equalsIgnoreCase(""))
             pickup_id =getStringValue(object,"pickup_id","");
             manager_name = getStringValue(object,"manager_name","N/A");
             order_id = getStringValue(object,"order_id","0");
             order_note = getStringValue(object,"order_note","");
             layout_id = getStringValue(object,"layout_name","N/A");
             tender =getStringValue(object,"tender","0");
             change =getStringValue(object,"change","0");
             cash_received =getStringValue(object,"cash_received","0");
             cash_returned =getStringValue(object,"cash_returned","0");
             tax_id = getStringValue(object,"tax_id","");
             product_discount =getStringValue(object,"product_discount","0");
             refund = getStringValue(object,"refund_amount","0");
             balance = getStringValue(object,"balance_amount","0");
             open = getStringValue(object,"open","1");
             promocode_amount = getStringValue(object,"promocode_amount","0");
             promocode_id = getStringValue(object,"promocode_id","0");
             order_discount_percentage = getStringValue(object,"order_discount_percentage","0");
             subsidized_amount = UIToolBox.getDoubleValue(object,"signcatch_promocode_amount");
             Log.e("Tender Amount", tender+"");
         } catch (JSONException e) {
             e.printStackTrace();
         }
     }

    public double getActual_amount() {
        return actual_amount;
    }

    public double getSubsidized_amount() {
        return subsidized_amount;
    }

    public String getProduct_discount() {
        return product_discount;
    }

    public String getPromocode_amount() {
        return promocode_amount;
    }

    public String getPromocode_id() {
        return promocode_id;
    }

    public int getTransaction_status() {
        return transaction_status;
    }

    public String getId() {
        return id;
    }

    public String getPayment_method() {
        return payment_method;
    }

    public String getUpdate_date() {
        return update_date;
    }

    public String getOrder_note() {
        return order_note;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAmount() {
        return amount;
    }

    public String getPayment_details() {
        return payment_details;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public void setCompany_name(String companyinfo) {
        try {
            JSONObject object = new JSONObject(companyinfo);
            if(object.has("company_name"))
            company_name = object.getString("company_name");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String getStringValue(JSONObject object,String key,String def) throws JSONException {
     return object.has(key) && !object.isNull(key) && !object.getString(key).equals("false")? object.getString(key):def;
    }
}
