package com.skyinnolabs.consumerapp;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Udit Paul-SignCatch on 7/16/2018.
 */

public class BharatQrDetails implements Serializable {
    private String trId;
    private String request_mid;
    private String txnID;
    private String transaction_amount;
    private String tid;
    private String ref_no;


    public BharatQrDetails(String response) throws JSONException {
        JSONObject jsonObject = new JSONObject(response);
        trId = getStringValue(jsonObject,"trId","0");
        request_mid = getStringValue(jsonObject,"request_mid","0");
        txnID =getStringValue(jsonObject,"txnID","0");
        transaction_amount =getStringValue(jsonObject,"transaction_amount","0");
        tid = getStringValue(jsonObject,"tid","N/A");
        ref_no = getStringValue(jsonObject,"ref_no","N/A");
    }

    public String getTrId() {
        return trId;
    }

    public String getRequest_mid() {
        return request_mid;
    }

    public String getTxnID() {
        return txnID;
    }

    public String getTransaction_amount() {
        return transaction_amount;
    }

    public String getTid() {
        return tid;
    }

    public String getRef_no() {
        return ref_no;
    }

    private String getStringValue(JSONObject object, String key, String def) throws JSONException {
        return object.has(key) && !object.isNull(key) && !object.getString(key).equals("false")? object.getString(key):def;
    }
}
