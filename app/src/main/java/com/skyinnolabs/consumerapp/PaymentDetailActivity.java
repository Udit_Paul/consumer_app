package com.skyinnolabs.consumerapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import io.socket.emitter.Emitter;

public class PaymentDetailActivity extends FragmentActivity implements OnAsycTaskCompletedListener, View.OnClickListener {
   private String response;
   View relLoading;
    CardView cardCreditDebit;
    CardView cardBharatQr;
    OrderInfo orderInfo;
    TextView tvScanMoreItem;
    Boolean payFlag = false;
    public FrameLayout frMain;
    public LinearLayout linMain;
    public PaymentDetailActivity() {
        // Required empty public constructor
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_payment_detail);

        tvScanMoreItem = findViewById(R.id.tvScanMoreItem);
        cardCreditDebit = findViewById(R.id.cardCreditDebit);
        cardBharatQr = findViewById(R.id.cardBharatQr);
        frMain = findViewById(R.id.frMain);
        relLoading = findViewById(R.id.relLoading);
        linMain = findViewById(R.id.linMain);

        tvScanMoreItem.setOnClickListener(this);
        cardCreditDebit.setOnClickListener(this);
        cardBharatQr.setOnClickListener(this);

        if(getIntent()!=null) {
            orderInfo = new OrderInfo(getIntent().getStringExtra("orderInfo"));
        }

        frMain.setVisibility(View.GONE);

        if(SigncatchPref.getBharatQrCredentials()!=null &&
                !SigncatchPref.getBharatQrCredentials().equalsIgnoreCase("") &&
                !SigncatchPref.getBharatQrCredentials().contains("false"))
            cardBharatQr.setVisibility(View.VISIBLE);
        else
            cardBharatQr.setVisibility(View.GONE);

    }

    public void addFragmentInMain(final Fragment fragment, final String tag, final Bundle bundle) {
        if (bundle != null)
            fragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().add(R.id.frMain, fragment, tag).commitAllowingStateLoss();
    }

    // Method for removing fragments
    public void removeFragment(String tag) {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction().remove(fragment).commit();
            getSupportFragmentManager().popBackStack();
            frMain.setVisibility(View.GONE);
        }
    }

    public void onClick(View v)
    {
       switch (v.getId())
       {
           case R.id.tvScanMoreItem:
               JSONObject object = null;
               try {
                   object = new JSONObject();
                   object.put("store",SigncatchPref.getStoreId());
                   object.put("type","cancel");
               } catch (JSONException e) {
                   e.printStackTrace();
               }
               if(SIGNCATCH.mSocket != null && SIGNCATCH.mSocket.connected())
                   SIGNCATCH.attemptSend(object);
               onBackPressed();
               break;
           case R.id.cardCreditDebit:
               cardBharatQr.setEnabled(false);
              if(NetworkManager.checkNetwork(PaymentDetailActivity.this))
              {
                  relLoading.setVisibility(View.VISIBLE);
               new MyNetworkAsyncTask(PaymentDetailActivity.this,"/order/check-status/"+orderInfo.getId()+"?access-token="+ SigncatchPref.getAccessToken()+"&type="+SigncatchPref.getLoginType(),44,this);
              }
              else
                  UIToolBox.setAlert(PaymentDetailActivity.this, "No network found, connect to internet and try again.");
              break;

           case R.id.cardBharatQr:
               cardCreditDebit.setEnabled(false);
               callBharatQrCodeDialog();
               break;
       }
      }

    private void sendRequest()
    {
        relLoading.setVisibility(View.GONE);
        cardCreditDebit.setEnabled(true);
        cardBharatQr.setEnabled(true);
        frMain.setVisibility(View.VISIBLE);
        if(SIGNCATCH.mSocket != null && SIGNCATCH.mSocket.connected() && SIGNCATCH.EZETAP)
        {
            Bundle bundle = new Bundle();
            bundle.putSerializable("detail",orderInfo);
            addFragmentInMain(new EzetapCardPaymentDialog(), "ezetap", bundle);
        }
        else
            UIToolBox.setAlert(PaymentDetailActivity.this,"Payment Device is not connected.");
    }

    private void callBharatQrCodeDialog() {
        if(NetworkManager.checkNetwork(PaymentDetailActivity.this))
        {
            relLoading.setVisibility(View.VISIBLE);
            String MID = "", TID = "";
            if(SigncatchPref.getBharatQrCredentials()!=null &&
                    !SigncatchPref.getBharatQrCredentials().equalsIgnoreCase("")) {
                try {
                    JSONObject object = new JSONObject(SigncatchPref.getBharatQrCredentials());
                    if (object.has("mid"))
                        MID = object.getString("mid");
                    if (object.has("tid"))
                        TID = object.getString("tid");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
//                progressView.setVisibility(View.VISIBLE);
//            button.setVisibility(View.GONE);
                Map<String, String> map = new HashMap<>();
                map.put("MID", MID);
                map.put("TID", TID);
                map.put("reference_id", orderInfo.getOrder_id());
                map.put("reference_type", "1");
                map.put("amount", UIToolBox.roundTwoDecimals(orderInfo.getRemaining_amount()));
                map.put("prgType", "5");
                map.put("qrType", "2");
                new MyNetworkAsyncTask(PaymentDetailActivity.this, getInventoryUrl(), 10,
                        map, PaymentDetailActivity.this);
            }
        } else {
            UIToolBox.setAlert(PaymentDetailActivity.this,"No network found, connect to internet and try again.");
        }
    }

    private String getInventoryUrl()
    {
        String url ="/payment/get-bharat-qr/"+SigncatchPref.getMerchantId()+"?access-token="+SigncatchPref.getAccessToken()+"&type="+SigncatchPref.getLoginType();
        return url;
    }

    @Override
    public void onTaskCompleted(final String response, int RequestCode) {
        if(this!=null && response!=null) {
            switch (RequestCode) {
                case 44:
                if (response.contains("transaction")) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.has("transaction")) {
                            if (jsonObject.getBoolean("transaction"))
                                setAlert(PaymentDetailActivity.this,"This order is already paid!");
                            else
                                sendRequest();
                        }
                    } catch (JSONException e) {
                        cardCreditDebit.setEnabled(true);
                        cardBharatQr.setEnabled(true);
                        e.printStackTrace();
                    }
                }
                break;

                case 10 :
                    relLoading.setVisibility(View.GONE);
                    SigncatchPref.setQrResponse(response);
                        SigncatchPref.setQrPayment(orderInfo.getAmount());
                    SIGNCATCH.setDataToDeviceLinstener(dataToDeviceLinstener);
                    Handler handler8 = new Handler();
                    handler8.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            linMain.setVisibility(View.GONE);
                            frMain.setVisibility(View.VISIBLE);
                            Log.e("BharatQr", "Called");
                            Bundle bundle = new Bundle();
                            bundle.putSerializable("orderInfo",orderInfo);
                            bundle.putString("response",response);
                            addFragmentInMain(new SmartBharatQrCodeFragment(), "Bharat Qr", bundle);
                        }
                    }, 300);
                    break;
                case 345 :
                        try {
                            relLoading.setVisibility(View.GONE);
                            if(response!=null && response.contains("Successfully Done")) {
                                JSONObject jsonObject = new JSONObject(response);
                                if (jsonObject.has("payment_details"))
                                    orderInfo.setPayments(jsonObject.getString("payment_details"));
                                if (jsonObject.has("bharat_qr_details"))
                                    orderInfo.setBharatQrDetails(jsonObject.getString("bharat_qr_details"));
                                if (jsonObject.has("remaining_amount")) {
                                    orderInfo.setRemaining_amount(jsonObject.getString("remaining_amount"));
                                }
                                if (jsonObject.has("refund_amount"))
                                    orderInfo.setRefund(jsonObject.getString("refund_amount"));
                                Toast.makeText(PaymentDetailActivity.this,"Payment Successfully Done!",Toast.LENGTH_LONG).show();
                                JSONObject object = new JSONObject();
                                object.put("type","transactionCompleted");
                                object.put("store",SigncatchPref.getStoreId());
                                if(SIGNCATCH.mSocket != null && SIGNCATCH.mSocket.connected())
                                    SIGNCATCH.attemptSend(object);
                                startActivity(new Intent(PaymentDetailActivity.this, ThankYouActivity.class));
                                finish();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
            }

        }
    }

    @Override
    public void onTaskFailed(String response, int RequestCode, int statusCode) {
        if(this!=null) {
            relLoading.setVisibility(View.GONE);
            cardCreditDebit.setEnabled(true);
            cardBharatQr.setEnabled(true);
            UIToolBox.errorAlert(PaymentDetailActivity.this, response, statusCode);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        try {
            if (intent != null && intent.hasExtra("response")) {
                Fragment fragment = getSupportFragmentManager().findFragmentByTag("ezetap");
                if (fragment instanceof OnEzetapCardPaymentListener) {
                    final OnEzetapCardPaymentListener onEzetapCardPaymentListener = (OnEzetapCardPaymentListener) fragment;
                    onEzetapCardPaymentListener.getResponseFromEzetap(intent.getStringExtra("response"),requestCode);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setAlert(Context context, String msg)
    {
        try {
            final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
// ...Irrelevant code for customizing the buttons and title
            LayoutInflater inflater = LayoutInflater.from(context);
            View dialogView = inflater.inflate(R.layout.alert_dialog_layout, null);
            TextView tvErrorMsg = (TextView) dialogView.findViewById(R.id.tvErrorMsg);
            ImageView ivClose = (ImageView) dialogView.findViewById(R.id.ivClose);
            tvErrorMsg.setText(msg);
            dialogBuilder.setView(dialogView);
            final AlertDialog alertDialog = dialogBuilder.create();

            ivClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    onBackPressed();
                }
            });
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        if(!payFlag)
            startActivity(new Intent(PaymentDetailActivity.this, MainActivity.class));
        else
            startActivity(new Intent(PaymentDetailActivity.this, ThankYouActivity.class));
        finish();
    }

    @Override
    protected void onDestroy() {
        SIGNCATCH.offDataToDeviceLinstener(dataToDeviceLinstener);
        super.onDestroy();
    }

    public Emitter.Listener dataToDeviceLinstener = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e("1st Response is",args[0]+" check");
                    if (args[0] != null) {
                        if (args[0].toString().contains("type")) {
                            try {
                                Log.e("Response is",args[0]+"");
                                JSONObject jsonObject = new JSONObject(args[0].toString());
                                if(jsonObject.getString("type").equalsIgnoreCase("bharatQRTransactionCompleted")) {
                                    Toast.makeText(PaymentDetailActivity.this, "Payment Completed!", Toast.LENGTH_SHORT).show();
                                            if(jsonObject.has("order") && !jsonObject.isNull("order") && !jsonObject.getString("order").equalsIgnoreCase("")) {
                                                JSONObject jsonObject1 = jsonObject.getJSONObject("order");
                                                if (jsonObject1.has("order_id") && jsonObject1.has("amount") && jsonObject1.has("reference_id")) {

                                                    if (jsonObject1.getString("order_id").equalsIgnoreCase(orderInfo.getOrder_id())) {
                                                        onCheckoutSuccess(jsonObject1.getString("reference_id"), jsonObject1.getString("amount"));
                                                    }
                                                }
/*                                        Intent intent = new Intent(PaymentDetailActivity.this,MainActivity.class);
                                        intent.putExtra("orderInfo",jsonObject.getString("order"));*/
                                        startActivity(new Intent(PaymentDetailActivity.this, ThankYouActivity.class));
                                        finish();
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            });
        }
    };

    private void onCheckoutSuccess(String reference_no, String amount) {
        if(PaymentDetailActivity.this!=null) {
            if (NetworkManager.checkNetwork(PaymentDetailActivity.this)) {
                Map<String, String> map = new HashMap<>();
                map.put("order_id", orderInfo.getId());
                map.put("transaction_status", "3");
                map.put("payment_type", "14");
                map.put("amount", amount);
                map.put("payment_detail", reference_no);
                map.put("reference_id", reference_no);

                map.put("override_access_info[" + 0 + "][manager_id]", SigncatchPref.getMerchantId());
                map.put("override_access_info[" + 0 + "][access_code]", "");
                if (orderInfo.getInstore().equals("1"))
                    map.put("override_access_info[" + 0 + "][override_type]", "retail_order");
                else if (orderInfo.getDinein().equals("1"))
                    map.put("override_access_info[" + 0 + "][override_type]", "dinein");
                else if (orderInfo.getPickup_detail().equals("1"))
                    map.put("override_access_info[" + 0 + "][override_type]", "pickup_order");
                else if (orderInfo.getDelivery_detail().equals("1")) {
                    if (orderInfo.getChannel() == 3)
                        map.put("override_access_info[" + 0 + "][override_type]", "whole_sale_order");
                    else if (orderInfo.getChannel() == 2)
                        map.put("override_access_info[" + 0 + "][override_type]", "online_order");
                    else
                        map.put("override_access_info[" + 0 + "][override_type]", "instore_order");
                } else
                    map.put("override_access_info[" + 0 + "][override_type]", "");
                map.put("override_access_info[" + 0 + "][action_type]", "payment");
                map.put("override_access_info[" + 0 + "][product_id]", "0");
                map.put("override_access_info[" + 0 + "][old_value]", "");
                map.put("override_access_info[" + 0 + "][new_value]", UIToolBox.roundTwoDecimals(orderInfo.getTranscationAmount()));
                new MyNetworkAsyncTask(PaymentDetailActivity.this, "/order/on-checkout-success?access-token=" + SigncatchPref.getAccessToken() + "&type=" + SigncatchPref.getLoginType(), 345, map, this);
            } else
                UIToolBox.setAlert(PaymentDetailActivity.this, "No network found, connect to internet and try again.");
        }
    }

}