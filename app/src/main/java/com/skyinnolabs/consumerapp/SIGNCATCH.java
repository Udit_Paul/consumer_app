package com.skyinnolabs.consumerapp;

import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.emitter.Emitter;

import static io.socket.client.Socket.EVENT_CONNECT;
import static io.socket.client.Socket.EVENT_DISCONNECT;
import static io.socket.engineio.client.Socket.EVENT_MESSAGE;

public class SIGNCATCH extends Application {

    static Context mContext;
    private static SIGNCATCH mInstance;
    public static SharedPreferences appSharedPreferences;
    public static boolean IS_DEBUGGING = true;
    public static boolean EZETAP = false;
    public  static io.socket.client.Socket mSocket;
    private WebSocketCommunicationLisntner webSocketListner;

    public static String  EVENT_RESPONSE_FROM_EDC ="responseFromEdc";
    public static String  EVENT_CHECK_EDC_RESPONSE ="check-edc-response";
    public static String  EVENT_DATA_TO_DEVICE="data_to_device";
    public static String  EVENT_PERMISSION_UPDATED="permissions-updated";
    public static String  EVENT_BHARAT_QR_PAYMENT="bharatQR-payment-done";

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        mContext = getApplicationContext();
        appSharedPreferences = this.getSharedPreferences("preferences", MODE_PRIVATE);
        getSocket();
    }

    public static Context getContext() {
        return mContext;
    }

    public io.socket.client.Socket getSocket()
    {
        try {
            mSocket = IO.socket(getString(R.string.notification_url));
            /*            mSocket = IO.socket("https://sandboxnotification.signcatch.com");*/
        } catch (URISyntaxException e) {
            System.out.println("message : "+ e.getMessage());
        }
        if(mSocket!=null) {
            Log.e("Emitter: ","Get socket connected");

            connectWebSocket();
        }
        return mSocket;
    }

    public void connectWebSocket() {
        if(mSocket!=null) {
            // checkAllLinetner();
            mSocket.off();
            mSocket.connect();
            mSocket.on(EVENT_CONNECT,onConnect);
            mSocket.on(EVENT_DISCONNECT,onDisconnect);
        }

    }
    public Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            registerDevice();
            Log.e("Emitter","Connected Called");
            checkAllLinetner();
            if(webSocketListner!=null)
                webSocketListner.onConnected(args);
        }
    };

    public void setWebSocketListner(WebSocketCommunicationLisntner webSocketListner) {
        this.webSocketListner = webSocketListner;
    }

    public static void setDataToDeviceLinstener(Emitter.Listener dataToDeviceLinstener)
    {
        if(mSocket!=null && !mSocket.hasListeners(EVENT_DATA_TO_DEVICE) && dataToDeviceLinstener!=null)
            mSocket.on(EVENT_DATA_TO_DEVICE, dataToDeviceLinstener);
        Log.e("DATA_TO_DEVICE-ON",mSocket.hasListeners(EVENT_DATA_TO_DEVICE)+"");
    }
    public static void offDataToDeviceLinstener(Emitter.Listener dataToDeviceLinstener)
    {
        if(mSocket!=null && mSocket.hasListeners(EVENT_DATA_TO_DEVICE) && dataToDeviceLinstener!=null)
            mSocket.off(EVENT_DATA_TO_DEVICE, dataToDeviceLinstener);
        Log.e("DATA_TO_DEVICE-OFF",mSocket.hasListeners(EVENT_DATA_TO_DEVICE)+"");
    }

    public static void setPermissionUpdatedLinstener(Emitter.Listener permissionUpdatedLinstener)
    {
        if(mSocket!=null && !mSocket.hasListeners(EVENT_PERMISSION_UPDATED) && permissionUpdatedLinstener!=null)
            mSocket.on(EVENT_PERMISSION_UPDATED, permissionUpdatedLinstener);
        Log.e("EVENT_PERMISSION-ON",mSocket.hasListeners(EVENT_PERMISSION_UPDATED)+"");
    }
    public static void offPermissionUpdatedLinstener(Emitter.Listener permissionUpdatedLinstener)
    {
        if(mSocket!=null && mSocket.hasListeners(EVENT_PERMISSION_UPDATED) && permissionUpdatedLinstener!=null)
            mSocket.off(EVENT_PERMISSION_UPDATED, permissionUpdatedLinstener);
        Log.e("EVENT_PERMISSION-OFF",mSocket.hasListeners(EVENT_PERMISSION_UPDATED)+"");
    }

    public static void setEventBharatQrPaymentListener(Emitter.Listener bharatQrPaymentLinstener)
    {
        if(mSocket!=null && !mSocket.hasListeners(EVENT_BHARAT_QR_PAYMENT) && bharatQrPaymentLinstener!=null)
            mSocket.on(EVENT_BHARAT_QR_PAYMENT, bharatQrPaymentLinstener);
        Log.e("EVENT_PERMISSION-ON",mSocket.hasListeners(EVENT_BHARAT_QR_PAYMENT)+"");
    }
    public static void offEventBharatQrPaymentListener(Emitter.Listener bharatQrPaymentLinstener)
    {
        if(mSocket!=null && mSocket.hasListeners(EVENT_BHARAT_QR_PAYMENT) && bharatQrPaymentLinstener!=null)
            mSocket.off(EVENT_BHARAT_QR_PAYMENT, bharatQrPaymentLinstener);
        Log.e("EVENT_PERMISSION-OFF",mSocket.hasListeners(EVENT_BHARAT_QR_PAYMENT)+"");
    }

    public static void setResponseFromEDC(Emitter.Listener responseFromEdc)
    {
        if(mSocket!=null && !mSocket.hasListeners(EVENT_RESPONSE_FROM_EDC) && responseFromEdc!=null)
            mSocket.on(EVENT_RESPONSE_FROM_EDC, responseFromEdc);
        Log.e("responseFromEdc-ON",mSocket.hasListeners(EVENT_RESPONSE_FROM_EDC)+"");
    }

    public static void offResponseFromEDC(Emitter.Listener responseFromEdc)
    {
        if(mSocket!=null && mSocket.hasListeners(EVENT_RESPONSE_FROM_EDC) && responseFromEdc!=null)
            mSocket.off(EVENT_RESPONSE_FROM_EDC, responseFromEdc);
        Log.e("responseFromEdc-OFF",mSocket.hasListeners(EVENT_RESPONSE_FROM_EDC)+"");
    }

    public static void setcheckNAddEdc(Emitter.Listener checkNAddEdc)
    {
        if(mSocket!=null && !mSocket.hasListeners(EVENT_CHECK_EDC_RESPONSE) && checkNAddEdc!=null)
            mSocket.on(EVENT_CHECK_EDC_RESPONSE, checkNAddEdc);
        Log.e("checkNAddEdc-ON",mSocket.hasListeners(EVENT_CHECK_EDC_RESPONSE)+"");
    }
    public static void offcheckNAddEdc(Emitter.Listener checkNAddEdc)
    {
        if(mSocket!=null && mSocket.hasListeners(EVENT_CHECK_EDC_RESPONSE) && checkNAddEdc!=null)
            mSocket.off(EVENT_CHECK_EDC_RESPONSE, checkNAddEdc);
        Log.e("checkNAddEdc-OFF",mSocket.hasListeners(EVENT_CHECK_EDC_RESPONSE)+"");
    }

    public Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.e("Emitter","Disconnected Called");
            checkAllLinetner();
            mSocket.off(EVENT_MESSAGE);
            if(webSocketListner!=null)
                webSocketListner.onDisconnected(args);
        }
    };

    public  void disconnectWebSocket()
    {
        if(mSocket!=null) {
            mSocket.disconnect();
            mSocket.off();
        }
    }

    public static  int  attemptSend(JSONObject jsonobj) {
        Log.e("Inside Socket",jsonobj.toString());
        if(mSocket!=null)
            mSocket.emit("FromA920ToPos",jsonobj);
        else
            return 0;
        return 1;
    }

    public static int registerDevice() {
        if(!SigncatchPref.getStoreId().equals("1")) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("storeId", SigncatchPref.getStoreId());
                jsonObject.put("posId", SigncatchPref.getDeviceName());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (mSocket != null) {
                Log.e("Emitter: ","Register-"+jsonObject.toString());
                System.out.println("register: "+jsonObject.toString());
                mSocket.emit("register", jsonObject);
            }
            else
                return 0;
            return 1;
        }
        else
            return 0;
    }


    private void checkAllLinetner()
    {
        if(mSocket!=null) {
            Log.e("Emitter",EVENT_CONNECT+" Count: "+mSocket.listeners(EVENT_CONNECT).size() + "");
            Log.e("Emitter",EVENT_DISCONNECT+" Count: "+mSocket.listeners(EVENT_DISCONNECT).size() + "");
            Log.e("Emitter",EVENT_RESPONSE_FROM_EDC+" Count: "+mSocket.listeners(EVENT_RESPONSE_FROM_EDC).size() + "");
            Log.e("Emitter",EVENT_MESSAGE+" Count: "+mSocket.listeners(EVENT_MESSAGE).size() + "");
            Log.e("Emitter",EVENT_DATA_TO_DEVICE+" Count: "+mSocket.listeners(EVENT_DATA_TO_DEVICE).size() + "");
            Log.e("Emitter",EVENT_CONNECT+": "+mSocket.hasListeners(EVENT_CONNECT) + "");
            Log.e("Emitter",EVENT_DISCONNECT+": "+mSocket.hasListeners(EVENT_DISCONNECT) + "");
            Log.e("Emitter",EVENT_RESPONSE_FROM_EDC+": "+mSocket.hasListeners(EVENT_RESPONSE_FROM_EDC) + "");
            Log.e("Emitter",EVENT_MESSAGE+": "+mSocket.hasListeners(EVENT_MESSAGE) + "");
            Log.e("Emitter",EVENT_DATA_TO_DEVICE+": "+mSocket.hasListeners(EVENT_DATA_TO_DEVICE) + "");
        }
    }

    public static void SHOW_SOFTKEYBOARD(View view) {
        try {
            ((InputMethodManager) view.getContext().getSystemService(Service.INPUT_METHOD_SERVICE))
                    .showSoftInput(view, 0);
        } catch (NullPointerException e) {
            Log.e("Error is", "null pointer exception");
        }
    }

    public static void HIDE_SOFTKEYBOARD(Activity activity) {
        try {
            ((InputMethodManager) activity.getSystemService(Service.INPUT_METHOD_SERVICE))
                    .hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        } catch (NullPointerException e) {

        }
    }

}
