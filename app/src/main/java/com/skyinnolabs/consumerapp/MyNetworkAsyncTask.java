package com.skyinnolabs.consumerapp;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Dharmpal Verma on 2/10/2017.
 */

public class MyNetworkAsyncTask extends AsyncTask<Void, Void, String> {
    private Context context;
    private String url;
    private Map<String, String> map;
    private boolean isGetRequest;
    private int requestCode;
    private OnAsycTaskCompletedListener listener;
    private boolean isExternal;
    private boolean isDelete;
    private String response;
    private int statusCode;
    private boolean isSuccessful;
    public MyNetworkAsyncTask(Context context, String url, int requestCode, Map<String, String> map, OnAsycTaskCompletedListener listener) {
        this.url = url;
        this.listener = listener;
        this.context = context;
        this.requestCode = requestCode;
        this.map = map;
        isDelete =false;
        if (map != null)
            isGetRequest = false;
        execute();
    }

    public MyNetworkAsyncTask(Context context, String url, int requestCode, Map<String, String> map, OnAsycTaskCompletedListener listener, boolean isExternal) {
        this.url = url;
        this.listener = listener;
        this.context = context;
        this.requestCode = requestCode;
        this.isExternal = isExternal;
        this.map = map;
        isDelete =false;
        if (map != null)
            isGetRequest = false;
        execute();
    }

    public MyNetworkAsyncTask(Context context, String url, int requestCode, boolean isDelete, OnAsycTaskCompletedListener listener) {
        this.url = url;
        this.listener = listener;
        this.context = context;
        this.requestCode = requestCode;
            isGetRequest = false;
          this.isDelete = true;
        execute();
    }

    public MyNetworkAsyncTask(Context context, String url, int requestCode, OnAsycTaskCompletedListener listener, boolean isExrernal) {
        this.url = url;
        this.listener = listener;
        this.context = context;
        this.requestCode = requestCode;
        this.isExternal = isExrernal;
        isGetRequest =true;
        isDelete =false;
        execute();
    }

    public MyNetworkAsyncTask(Context context, String url, int requestCode, OnAsycTaskCompletedListener listener) {
        //onAttach(context);
        this.url = url;
        this.listener = listener;
        this.context = context;
        this.requestCode = requestCode;
        isGetRequest = true;
        isDelete =false;
        execute();
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(Void... params) {

        if(SIGNCATCH.IS_DEBUGGING)
            Log.e("SCNetwork","Api Url :"+getUrl(url));
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();

        if (isGetRequest) {
            Request request = new Request.Builder().url(getUrl(url)).build();
            try {
                Response httpresponse = client.newCall(request).execute();
                response = httpresponse.body().string();
                isSuccessful =httpresponse.isSuccessful();
                statusCode =httpresponse.code();
             }  catch (Exception e) {
                e.printStackTrace();
                response = null;
                isSuccessful = false;
                statusCode = 0;
                return null;
            }
        }
        else if(isDelete)
        {
            Request request = new Request.Builder().url(getUrl(url)).delete().build();
            try {
               Response httpresponse = client.newCall(request).execute();

                response =httpresponse.code()+"";
                isSuccessful =httpresponse.isSuccessful();
                statusCode =httpresponse.code();
            } catch (IOException e) {
                e.printStackTrace();
                response = null;
                isSuccessful = false;
                statusCode = 0;
            }
        }
        else {

            if (url != null && !url.contains("get-bharat-qr") && !url.contains("check-status")) {

                FormBody.Builder builder = new FormBody.Builder();

                for (Map.Entry<String, String> entry : map.entrySet()) {
                    if (SIGNCATCH.IS_DEBUGGING)
                        Log.e("SCNetwork", " Api Params :  " + entry.getKey() + " : " + entry.getValue());
                    if (entry.getKey() != null && entry.getValue() != null)
                        builder.add(entry.getKey(), entry.getValue());
                }
                try {
                    RequestBody formBody = builder.build();
                    Request request = new Request.Builder().url(getUrl(url)).post(formBody).build();
                    Response httpresponse = client.newCall(request).execute();
                    response = httpresponse.body().string();
                    isSuccessful = httpresponse.isSuccessful();
                    statusCode = httpresponse.code();
                } catch (IOException e) {
                    e.printStackTrace();
                    response = null;
                    isSuccessful = false;
                    statusCode = 0;
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            } else {
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                JSONObject json = new JSONObject();
                for (Map.Entry<String, String> entry : map.entrySet()) {
                    if (SIGNCATCH.IS_DEBUGGING)
                        Log.e("SCNetwork", " Api Params :  " + entry.getKey() + " : " + entry.getValue());
                    if (entry.getKey() != null && entry.getValue() != null)
                        try {
                            json.put(entry.getKey(), entry.getValue());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                }
                RequestBody body = RequestBody.create(JSON, json.toString());
                Request request = new Request.Builder()
                        .url(getUrl(url))
                        .post(body)
                        .build();
                try {
                    Response httpresponse = client.newCall(request).execute();
                    response = httpresponse.body().string();
                    isSuccessful = httpresponse.isSuccessful();
                    statusCode = httpresponse.code();
                } catch (IOException e) {
                    e.printStackTrace();
                    response = null;
                    isSuccessful = false;
                    statusCode = 0;
                }
            }
        }

        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if(SIGNCATCH.IS_DEBUGGING)
            Log.e("SCNetwork ","Api Response :  "+response);
        if(listener!=null) {
            if (response != null) {
                if (isSuccessful)
                    listener.onTaskCompleted(response, requestCode);
                else
                    listener.onTaskFailed(response, requestCode, statusCode);
            }
            else
                listener.onTaskFailed(response, requestCode, statusCode);
        }
    }
    private String getUrl(String url) {
        if(url.contains("/master/update-product-batch-location"))
            return url;
        else if(url.contains("/master/add-product-info"))
            return url;
        if (context != null && !isExternal) {
            if (url.contains("/login?device_id=") || url.contains("/void-axis-transaction"))
                return context.getString(R.string.basic_url) + url;
            else if (url.contains("/mainLogin"))
                return url;
            else
                return context.getString(R.string.basic_url) + url + "&device_id=" + SigncatchPref.getDeviceName() + "&device_type=3";
        }
        else if(isExternal)
            return url;
         else
            return "";
    }

}
