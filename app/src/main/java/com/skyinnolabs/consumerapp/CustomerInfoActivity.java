package com.skyinnolabs.consumerapp;

import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import io.socket.emitter.Emitter;

public class CustomerInfoActivity extends Activity {

    EditText etUserName;
    EditText etEmail;
    TextView tvUserName;
    TextView tvInfoLabel;
    TextView tvScanMoreItem;
    Button btnContinue;
    LinearLayout linCustomerInfo;
    String userInfo = "", userNo = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_customer_info);

        if(getIntent()!=null) {
            userInfo = getIntent().getStringExtra("userInfo");
            userNo = getIntent().getStringExtra("userNo");
        }

        etUserName = findViewById(R.id.fragCustomerEditTextUserName);
        etEmail = findViewById(R.id.fragCustomerEditTextEmail);
        tvUserName = findViewById(R.id.tvUserName);
        tvInfoLabel = findViewById(R.id.tvInfoLabel);
        btnContinue = findViewById(R.id.fragCustomerButtonContinue);
        linCustomerInfo = findViewById(R.id.linCustomerInfo);
        tvScanMoreItem = findViewById(R.id.tvScanMoreItem);

        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etUserName.getText().toString().trim().equalsIgnoreCase(""))
                    etUserName.setError("It's a mandatory field");
                else {
                    try {
                        SIGNCATCH.HIDE_SOFTKEYBOARD(CustomerInfoActivity.this);
                        JSONObject object = new JSONObject();
                        JSONObject objOrder = new JSONObject(userInfo);
                        object.put("user", objOrder);
                        object.put("number", userNo);
                        object.put("name", etUserName.getText().toString().trim());
                        object.put("mail", etEmail.getText().toString().trim());
                        object.put("store",SigncatchPref.getStoreId());
                        object.put("type","customer");
                        object.put("userFound","0");
                            String uName = etUserName.getText().toString().trim();
                            String[] name = uName.split(" ");
                            if (name.length > 1) {
                                String fName = uName.replace(name[name.length - 1], "");
                                tvUserName.setText("Hi "+fName);
                                tvUserName.setTextColor(Color.parseColor("#008577"));
                                object.put("first_name", fName);
                                object.put("last_name",name[name.length - 1]);
                            } else {
                                object.put("first_name", uName);
                                object.put("last_name","");
                                tvUserName.setText("Hi "+uName);
                                tvUserName.setTextColor(Color.parseColor("#008577"));
                            }
                            tvInfoLabel.setText("Your Billing is in progress...");
                        linCustomerInfo.setVisibility(View.GONE);
                        tvScanMoreItem.setVisibility(View.VISIBLE);
                        SIGNCATCH.setDataToDeviceLinstener(dataToDeviceLinstener);
                        if(SIGNCATCH.mSocket != null && SIGNCATCH.mSocket.connected()) {
                            SIGNCATCH.attemptSend(object);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        tvScanMoreItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    JSONObject object = new JSONObject();
                    object.put("store",SigncatchPref.getStoreId());
                    object.put("type","cancel");
                    if(SIGNCATCH.mSocket != null && SIGNCATCH.mSocket.connected())
                        SIGNCATCH.attemptSend(object);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                startActivity(new Intent(CustomerInfoActivity.this, MainActivity.class));
            }
        });

        etUserName.requestFocus();
        InputMethodManager imm = (InputMethodManager)getSystemService(Service.INPUT_METHOD_SERVICE);
        try {
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        } catch (Exception e) {
            Log.e("Error is", "Null pointer exception");
        }

    }

    @Override
    protected void onDestroy() {
        SIGNCATCH.offDataToDeviceLinstener(dataToDeviceLinstener);
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(CustomerInfoActivity.this, MainActivity.class));
        finish();
    }

    public Emitter.Listener dataToDeviceLinstener = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e("1st Response is",args[0]+" check");
                    if (args[0] != null) {
                        if (args[0].toString().contains("type")) {
                            try {
                                Log.e("Response is",args[0]+"");
                                JSONObject jsonObject = new JSONObject(args[0].toString());
                                if(jsonObject.getString("type").equalsIgnoreCase("transactionStarted")) {
                                    Toast.makeText(CustomerInfoActivity.this, "Begin Transaction!", Toast.LENGTH_SHORT).show();
                                    if(jsonObject.has("order") && !jsonObject.isNull("order") && !jsonObject.getString("order").equalsIgnoreCase("")) {
                                        Intent intent = new Intent(CustomerInfoActivity.this,PaymentDetailActivity.class);
                                        intent.putExtra("orderInfo",jsonObject.getString("order"));
                                        startActivity(intent);
                                        finish();
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            });
        }
    };

}
