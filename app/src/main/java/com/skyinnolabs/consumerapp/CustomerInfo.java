package com.skyinnolabs.consumerapp;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Dharmpal Verma on 2/21/2017.
 */

public class CustomerInfo implements Serializable {
    private String userId="";
    private String customerName="";
    private String fname="";
    private String lname="";
    private String customerEmail="";
    private String customerPhone="";
    private String countryCode="";
    private String cover;
    private String gstId="";
    private boolean checkFlag = false;

    public CustomerInfo(String customerName, String customerEmail, String customerPhone,
                        String countrycode,String cover,String gstId, boolean checkFlag) {
        this.customerName = customerName;
        this.customerEmail = customerEmail;
        this.customerPhone = customerPhone;
        this.countryCode = countrycode;
        this.cover = cover;
        this.gstId = gstId;
        this.checkFlag = checkFlag;
    }

    public CustomerInfo(String response) {
        if(response!=null && !response.equalsIgnoreCase("")) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                fname = UIToolBox.getStringValue(jsonObject, "first_name", "");
                lname = UIToolBox.getStringValue(jsonObject, "last_name", "");
                customerName = fname + " " + lname;
                userId = UIToolBox.getStringValue(jsonObject, "id", "");
                customerEmail = UIToolBox.getStringValue(jsonObject, "email", "");
                countryCode = UIToolBox.getStringValue(jsonObject, "isd_code", "");
                gstId = UIToolBox.getStringValue(jsonObject, "gst_id", "");
                customerPhone = UIToolBox.getStringValue(jsonObject, "phone_number", "");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public String getFname() {
        return fname;
    }

    public String getLname() {
        return lname;
    }

    public boolean isCheckFlag() {
        return checkFlag;
    }

    public void setCheckFlag(boolean checkFlag) {
        this.checkFlag = checkFlag;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public void setGstId(String gstId) {
        this.gstId = gstId;
    }

    public String getCover() {
        return cover;
    }

    public String getGstId() {
        return gstId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public String getCountryCode() {
        return countryCode;
    }
}
