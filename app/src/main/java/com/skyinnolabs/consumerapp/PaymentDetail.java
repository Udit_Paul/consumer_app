package com.skyinnolabs.consumerapp;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Dharmpal Verma on 9/18/2017.
 */

public class PaymentDetail implements Serializable {
     private String id;
     private String order_id;
     private String amount;
     private String payment_status;
     private String payment_type;
     private String payment_method;
     private String payment_detail;
     private String cash_received;
     private String cash_returned;
     private String reference_id;


    public PaymentDetail(String response) throws JSONException {
        JSONObject jsonObject = new JSONObject(response);
       id = getStringValue(jsonObject,"id","0");
       order_id = getStringValue(jsonObject,"order_id","0");
       amount =getStringValue(jsonObject,"amount","0");
       payment_status =getStringValue(jsonObject,"payment_status","0");
       payment_method = getStringValue(jsonObject,"payment_method","N/A");
       payment_detail = getStringValue(jsonObject,"payment_detail","N/A");
        payment_type = getStringValue(jsonObject,"payment_type","0");
        cash_received = getStringValue(jsonObject,"cash_received","0");
        cash_returned = getStringValue(jsonObject,"cash_returned","0");
        reference_id = getStringValue(jsonObject, "reference_id", "");
    }

    public String getId() {
        return id;
    }

    public String getOrder_id() {
        return order_id;
    }

    public String getAmount() {
        return amount;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public String getPayment_method() {
        return payment_method;
    }

    public String getPayment_detail() {
        return payment_detail;
    }

    public String getCash_received() {
        return cash_received;
    }

    public String getCash_returned() {
        return cash_returned;
    }

    public String getReference_id() {
        return reference_id;
    }

    private String getStringValue(JSONObject object, String key, String def) throws JSONException {
        return object.has(key) && !object.isNull(key) && !object.getString(key).equals("false")? object.getString(key):def;
    }
}
