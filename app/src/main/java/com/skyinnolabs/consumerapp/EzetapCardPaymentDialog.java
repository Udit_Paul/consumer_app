package com.skyinnolabs.consumerapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.eze.api.EzeAPI;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import io.socket.emitter.Emitter;

public class EzetapCardPaymentDialog extends Fragment implements OnAsycTaskCompletedListener, OnEzetapCardPaymentListener {
    Context context;
    private String orderid="";
    private boolean isActionTaken;
    private OrderInfo detail ;
    private final int REQUEST_CODE_SALE_TXN = 10006;
    private final int REQUEST_CODE_INITIALIZE = 10001;
    private final int REQUEST_CODE_CLOSE = 10014;
    private EditText customerNameEditText;
    private EditText emailIdEditText;
    private EditText mobileNumberEditText;
    private EditText orderNumberEditText;
    private EditText payableAmountEditText;
    private EditText cashBackAmountEditText;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.payment_payload_popup, container, false);
        intialize(v);
        return v;
    }

    @Override
    public void onAttach(Context context) {
        this.context =context;
        super.onAttach(context);
    }

    private void intialize(View view) {

        if (getArguments() != null) {
            Bundle bundle = getArguments();
            detail = (OrderInfo) bundle.getSerializable("detail");
/*            orderid = detail.getId();
            if(!ledgerFlag) {
*//*                mTransactionData.mBaseAmount = UIToolBox.roundTwoDecimals(detail.getTranscationAmount());
                mTransactionData.mTotAmount = UIToolBox.roundTwoDecimals(detail.getTranscationAmount());
                mTransactionData.mPhoneNo = "+919818936242";
                mTransactionData.mReceipt = orderid;
                mTransactionData.mExtraNote1 = detail.getCustomer_name();
                tvMSG.setText("You will be paying Rs " + mTransactionData.mTotAmount + " using your card.");
                tvMSG1.setText("You will be paying Rs " + mTransactionData.mTotAmount + " using your card.");
                Log.e("Total Amount: ", mTransactionData.mTotAmount);*//*
            } else {
*//*                mTransactionData.mBaseAmount = UIToolBox.roundTwoDecimals(summaryDetail.getEnteredAmount());
                mTransactionData.mTotAmount = UIToolBox.roundTwoDecimals(summaryDetail.getEnteredAmount());
                mTransactionData.mPhoneNo = "+919818936242";
                mTransactionData.mReceipt = summaryDetail.getCustomer_id();
                mTransactionData.mExtraNote1 = summaryDetail.getFirst_name().trim()+" "+summaryDetail.getLast_name().trim();
                tvMSG.setText("You will be paying Rs " + mTransactionData.mTotAmount + " using your card.");
                tvMSG1.setText("You will be paying Rs " + mTransactionData.mTotAmount + " using your card.");
                Log.e("Total Amount: ", mTransactionData.mTotAmount);*//*
            }*/
        }

        Button cancelButton = (Button) view.findViewById(R.id.cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doCloseEzetap();
                removeFrag(false);
//                alertDialog.cancel();
            }
        });

        customerNameEditText = (EditText) view.findViewById(R.id.user_name);
        emailIdEditText = (EditText) view.findViewById(R.id.user_email);
        mobileNumberEditText = (EditText) view.findViewById(R.id.user_mobile);
        orderNumberEditText = (EditText) view.findViewById(R.id.order_number);
        payableAmountEditText = (EditText) view.findViewById(R.id.payable_amount);
        cashBackAmountEditText = (EditText) view.findViewById(R.id.cashback_amount);

            orderid = detail.getId();
            customerNameEditText.setText(detail.getCustomer_name());
            emailIdEditText.setText(detail.getCustomer_email());
            mobileNumberEditText.setText(detail.getCustomer_phone_no());
            orderNumberEditText.setText(orderid);
            payableAmountEditText.setText(detail.getTranscationAmount()+"");

        Button confirmButton = (Button) view.findViewById(R.id.confirm_button);
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doInitializeEzeTap();
//                    alertDialog.cancel();
            }
        });

        doInitializeEzeTap();

    }

    private void doInitializeEzeTap() {
        /**********************************************
         {
         "demoAppKey": "your demo app key",
         "prodAppKey": "your prod app key",
         "merchantName": "your merchant name",
         "userName": "your user name",
         "currencyCode": "INR",
         "appMode": "DEMO/PROD",
         "captureSignature": "true/false",
         "prepareDevice": "true/false"
         }
         **********************************************/
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("demoAppKey", "4dcaea74-ac01-4b4c-9550-0c5fdc3a7cf5");
            jsonRequest.put("prodAppKey", "4dcaea74-ac01-4b4c-9550-0c5fdc3a7cf5");
            jsonRequest.put("merchantName", "SIGNCATCH_437183");
            jsonRequest.put("userName", "2222180001");
            jsonRequest.put("currencyCode", "INR");
            jsonRequest.put("appMode", "DEMO");
            jsonRequest.put("captureSignature", "false");
            jsonRequest.put("prepareDevice", "true");
            EzeAPI.initialize(getActivity(), REQUEST_CODE_INITIALIZE, jsonRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void callPaymentAPI() {
        if (orderNumberEditText.getText().toString().equalsIgnoreCase("")
                || payableAmountEditText.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(context, "Please fill up mandatory params.",Toast.LENGTH_SHORT).show();
            return;
        }
        try {
            JSONObject jsonRequest = new JSONObject();
            JSONObject jsonOptionalParams = new JSONObject();
            JSONObject jsonReferences = new JSONObject();
            JSONObject jsonCustomer = new JSONObject();

                orderid = detail.getId();
                //Amount (mandatory)
                jsonRequest.put("amount", detail.getTranscationAmount()+"");

                if(detail.getCustomer_name()!=null && !detail.getCustomer_name().equalsIgnoreCase("")) {
                    // Building Customer Object(Optional)
                    jsonCustomer.put("name", detail.getCustomer_name());
                    jsonCustomer.put("mobileNo", detail.getCustomer_phone_no());
                    jsonCustomer.put("email", detail.getCustomer_email());
                    jsonOptionalParams.put("customer", jsonCustomer);
                }

                // Building References Object(Optional)
                // Additionally reference2 and reference3 can be used as well
                if(detail.getPaymentList()!=null && detail.getPaymentList().size()>0) {
                    Log.e("Payment List",detail.getPaymentList().size()+" not null");
                    jsonReferences.put("reference1", detail.getOrder_id() + "_" + detail.getPaymentList().size()+1);
                    jsonReferences.put("reference2", orderid);
                } else if(detail.getPaymentList()!=null && detail.getPaymentList().size()<=0) {
//                    Log.e("Payment List",detail.getPaymentList().size()+" not null");
                    jsonReferences.put("reference1", detail.getOrder_id() + "_1");
                    jsonReferences.put("reference2", orderid);
                } else if(detail.getPaymentList()==null) {
//                    Log.e("Payment List",detail.getPaymentList().size()+" not null");
                    jsonReferences.put("reference1", detail.getOrder_id() + "_1");
                    jsonReferences.put("reference2", orderid);
                }
/*            //Amount (mandatory)
            jsonRequest.put("amount", payableAmountEditText.getText().toString().trim());

            // Building Customer Object(Optional)
            jsonCustomer.put("name", customerNameEditText.getText().toString().trim());
            jsonCustomer.put("mobileNo", mobileNumberEditText.getText().toString().trim());
            jsonCustomer.put("email", emailIdEditText.getText().toString().trim());
            jsonOptionalParams.put("customer", jsonCustomer);

            // Building References Object(Optional)
            // Additionally reference2 and reference3 can be used as well
            jsonReferences.put("reference1", orderNumberEditText.getText().toString().trim());*/

            // Passing Additional References(Optional)
            // If reference1, reference2 & reference3 is insufficient then additionalReferences array can be used
            JSONArray array = new JSONArray();
            array.put("addRef_xx1");
            array.put("addRef_xx2");
            jsonReferences.put("additionalReferences", array);

            jsonOptionalParams.put("references", jsonReferences);

            // Building Optional params Object(Optional)
            //jsonOptionalParams.put("amountCashback", cashBackAmountEditText.getText().toString() + "");// Cannot

            // Tip (Optional)
            //jsonOptionalParams.put("amountTip", 0.00);

            // Pay to Account(Optional) - Used for Multi TID payments
            //jsonOptionalParams.put("payToAccount", "12322");

            //Additional Data(Optional) - Used to send any additional info
            //Rarely used and limited for Ezetap's internal use
            JSONObject addlData = new JSONObject();
            addlData.put("addl1", "addl1");
            addlData.put("addl2", "addl2");
            addlData.put("addl3", "addl3");
            //jsonOptionalParams.put("addlData", addlData);

            //Application Data(Optional) - Used to send any app info
            //Rarely used and limited for Ezetap's internal use
            JSONObject appData = new JSONObject();
            appData.put("app1", "app1");
            appData.put("app2", "app2");
            appData.put("app3", "app3");
            //jsonOptionalParams.put("appData", appData);

            // Building final optional params
            jsonRequest.put("options", jsonOptionalParams);

            InputMethodManager imm = (InputMethodManager) getActivity()
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(emailIdEditText.getWindowToken(), 0);

/*                            case REQUEST_CODE_WALLET_TXN:
                                doWalletTxn(jsonRequest);
                                break;*/
/*                            case REQUEST_CODE_CHEQUE_TXN:
                                // Building Cheque Object
                                JSONObject jsonCheque = new JSONObject();
                                jsonCheque.put("chequeNumber", "");
                                jsonCheque.put("bankCode", "");
                                jsonCheque.put("bankName", "");
                                jsonCheque.put("bankAccountNo", "");
                                jsonCheque.put("chequeDate", "");

                                jsonRequest.put("cheque", jsonCheque);

                                doChequeTxn(jsonRequest);
                                break;*/
            jsonRequest.put("mode", "SALE");//Card payment Mode(mandatory)
            doSaleTxn(jsonRequest);
/*                            case REQUEST_CODE_CASH_BACK_TXN:
                                jsonRequest.put("mode", "CASHBACK");//Card payment Mode(mandatory)
                                doCashbackTxn(jsonRequest);
                                break;
                            case REQUEST_CODE_CASH_AT_POS_TXN:
                                jsonRequest.put("mode", "CASH@POS");//Card payment Mode(mandatory)
                                doCashAtPosTxn(jsonRequest);
                                break;
                            case REQUEST_CODE_CASH_TXN:
                                doCashTxn(jsonRequest);
                                break;*/
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void doSaleTxn(JSONObject jsonRequest) {
        /******************************************
         {
         "amount": "123",
         "options": {
         "amountCashback": 0,
         "amountTip": 0,
         "references": {
         "reference1": "1234",
         "additionalReferences": [
         "addRef_xx1",
         "addRef_xx2"
         ]
         },
         "customer": {
         "name": "xyz",
         "mobileNo": "1234567890",
         "email": "abc@xyz.com"
         }
         },
         "mode": "SALE"
         }
         ******************************************/
        EzeAPI.cardTransaction(getActivity(), REQUEST_CODE_SALE_TXN, jsonRequest);
    }

    private void doCloseEzetap() {
        EzeAPI.close(getActivity(), REQUEST_CODE_CLOSE);
    }

    @Override
    public void getResponseFromEzetap(String response, int requestCode) {
        Log.e("SampleAppLogs", requestCode + " , " + response);
        switch (requestCode) {
            case REQUEST_CODE_INITIALIZE:
                Log.e("Device Initialize Log", response);
                callPaymentAPI();
                break;
            case REQUEST_CODE_SALE_TXN:
                if (getActivity() != null && NetworkManager.checkNetwork(getActivity())) {
                    Log.e("Device Payment Log", response);
                    final Map<String,String> map = new HashMap<>();
                    try {
                        JSONObject obj = new JSONObject(response);

                        if(response!=null && obj.has("status")) {
                            if(obj.getString("status").equalsIgnoreCase("success")) {

                                if(obj.has("result") && !obj.isNull("result")) {
                                    JSONObject obj10 = obj.getJSONObject("result");
                                    if (obj10.has("txn") && !obj10.isNull("txn")) {
                                        JSONObject obj1 = obj10.getJSONObject("txn");
                                        // Method for sending response to server
                                        map.put("order_id", detail.getId());
                                        map.put("transaction_status", "3");
                                        map.put("payment_type", "3");
/*                                    map.put("card_type","DEBIT");
                                    map.put("payment_detail","4444");
                                    map.put("transaction_id","123456");
                                    map.put("stan_id","123456");
                                    map.put("rr_no","123456");
                                    if(!ledgerFlag)
                                        map.put("amount",detail.getAmount());
                                    else
                                        map.put("amount",summaryDetail.getEnteredAmount());*/
                                        map.put("payment_channel_type", "4");
/*                                    map.put("auth_code","123456");
                                    map.put("txn_date","2018-11-26T14:55:07+0530");
                                    map.put("mid","123456");
                                    map.put("tid","123456");*/
                                        map.put("transaction_id",obj1.getString("txnId"));
                                        map.put("txnId", obj1.getString("txnId"));
                                        map.put("success", "1");
                                        map.put("mid", obj1.getString("mid"));
                                        map.put("currencyCode", obj1.getString("currencyCode"));
                                        map.put("amount", obj1.getString("amount"));
                                        map.put("rrNumber", obj1.getString("rrNumber"));
                                        map.put("stan", "");
                                        map.put("authCode", obj1.getString("authCode"));
                                        map.put("deviceSerial", obj1.getString("deviceSerial"));

                                        if (obj1.has("addlData") && !obj1.isNull("addlData")) {
                                            JSONObject obj2 = obj1.getJSONObject("addlData");
                                            // Method for sending response to server
                                            try {
                                                map.put("nameOnCard", obj2.getString("nameOnCard")
                                                        .substring(0, obj2.getString("nameOnCard").length() - 2));
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                                map.put("nameOnCard", "");
                                            }
                                            map.put("cardLastFourDigit", obj2.getString("cardLastFourDigit"));
                                            map.put("payment_detail", obj2.getString("cardLastFourDigit"));
                                            map.put("txnType", obj2.getString("txnType"));
                                            map.put("cardTxnType", obj2.getString("cardTxnTypeDesc"));
                                            map.put("customerReceiptUrl", obj2.getString("receiptUrl"));
                                            map.put("readableChargeSlipDate", obj2.getString("readableChargeSlipDate"));
                                            map.put("reverseReferenceNumber", obj2.getString("reverseReferenceNumber"));
                                        }

                                        if (obj10.has("customer") && !obj10.isNull("customer")) {
                                            JSONObject obj2 = obj10.getJSONObject("customer");
                                            map.put("customerName", obj2.getString("name"));
                                        }
                                        if (obj10.has("merchant") && !obj10.isNull("merchant")) {
                                            JSONObject obj2 = obj10.getJSONObject("merchant");
                                            map.put("merchantName", obj2.getString("merchantName"));
                                        }
                                        if (obj10.has("cardDetails") && !obj10.isNull("cardDetails")) {
                                            JSONObject obj2 = obj10.getJSONObject("cardDetails");
                                            map.put("paymentCardType", obj2.getString("cardType"));
                                            map.put("paymentCardBrand", obj2.getString("cardBrand"));
                                            map.put("card_type", obj2.getString("cardType"));
                                        }
                                        if (obj10.has("references") && !obj10.isNull("references")) {
                                            JSONObject obj2 = obj10.getJSONObject("references");
                                            map.put("externalRefNumber2", detail.getId());
                                            map.put("externalRefNumber", detail.getOrder_id());
                                        }
                                    }

                                        map.put("override_access_info[" + 0 + "][manager_id]", SigncatchPref.getMerchantId());
                                        if (detail.getInstore().equals("1"))
                                            map.put("override_access_info[" + 0 + "][override_type]", "retail_order");
                                        else if (detail.getDinein().equals("1"))
                                            map.put("override_access_info[" + 0 + "][override_type]", "dinein");
                                        else if (detail.getPickup_detail().equals("1"))
                                            map.put("override_access_info[" + 0 + "][override_type]", "pickup_order");
                                        else if (detail.getDelivery_detail().equals("1"))
                                            map.put("override_access_info[" + 0 + "][override_type]", "delivery_order");
                                        else
                                            map.put("override_access_info[" + 0 + "][override_type]", "");
                                        map.put("override_access_info[" + 0 + "][action_type]", "payment");
                                        map.put("override_access_info[" + 0 + "][product_id]", "0");
                                        map.put("override_access_info[" + 0 + "][new_value]", UIToolBox.roundTwoDecimals(detail.getTranscationAmount()));
                                    new MyNetworkAsyncTask(getActivity(), "/order/on-checkout-success?access-token=" + SigncatchPref.getAccessToken() + "&type=" + SigncatchPref.getLoginType(), 34, map, EzetapCardPaymentDialog.this);
                                }
                            } else {
                                removeFrag(false);
                            }
                        } else {
                            removeFrag(false);
                        }
/*                        new MyNetworkAsyncTask(getActivity(), "/order/on-checkout-success?access-token=" + SigncatchPref.getAccessToken() + "&type=" + SigncatchPref.getLoginType(), 34, map, EzetapCardPaymentDialog.this);*/
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case REQUEST_CODE_CLOSE:
                Log.e("Device Close Log", response);
                break;
        }
    }

    @Override
    public void onTaskCompleted(String response, int RequestCode) {
        if (this != null && this.isVisible()) {
            doCloseEzetap();
            switch (RequestCode) {
                case 34:
                    if (response != null && response.contains("Successfully Done")) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.has("payment_details"))
                                detail.setPayments(jsonObject.getString("payment_details"));
                            if (jsonObject.has("bharat_qr_details"))
                                detail.setBharatQrDetails(jsonObject.getString("bharat_qr_details"));
                            if (jsonObject.has("remaining_amount")) {
                                detail.setRemaining_amount(jsonObject.getString("remaining_amount"));
                            }
                            if (jsonObject.has("refund_amount"))
                                detail.setRefund(jsonObject.getString("refund_amount"));
                                Bundle bundle = new Bundle();
                                bundle.putSerializable("order",detail);
                            JSONObject object = new JSONObject();
                            object.put("type","transactionCompleted");
                            object.put("store",SigncatchPref.getStoreId());
                            int res = -1;
                            if(SIGNCATCH.mSocket != null && SIGNCATCH.mSocket.connected())
                                res = SIGNCATCH.attemptSend(object);
                            Log.e("Response 3",res+" check");
                                /*                       addFragmentInMain(new SelectPaymentDialog(),"Select payment",bundle);*/
/*                                SplitPaymentDialog dialog = SplitPaymentDialog.buildInfo(detail, accessFlag);
                                dialog.setCancelable(true);
                                try {
                                    dialog.show(getActivity().getSupportFragmentManager(), "Split payment");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }*/

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        UIToolBox.setAlert(getActivity(), "Your Payment is done successfully but due to some internal issue your order is not yet completed. Please contact staff to make your order complete.");
                        JSONObject object = null;
                        try {
                            object = new JSONObject();
                            object.put("type","transactionCompleted");
                            object.put("store",SigncatchPref.getStoreId());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        int res = -1;
                        if(SIGNCATCH.mSocket != null && SIGNCATCH.mSocket.connected())
                            res = SIGNCATCH.attemptSend(object);
                        Log.e("Response 4",res+" check");
                    }
                    removeFrag(true);
                    break;
            }
        }
    }

    @Override
    public void onTaskFailed(String response, int RequestCode, int statusCode) {
        if(this!=null && this.isVisible()) {
            UIToolBox.errorAlert(getActivity(), response, statusCode);
            JSONObject object = null;
            try {
                object = new JSONObject();
                object.put("type","transactionCancelled");
                object.put("store",SigncatchPref.getStoreId());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            int res = -1;
            if(SIGNCATCH.mSocket != null && SIGNCATCH.mSocket.connected())
                res = SIGNCATCH.attemptSend(object);
            Log.e("Response 5",res+" check");
            removeFrag(false);
        }
    }

    private void removeFrag(boolean payFlag)
    {
        if(getActivity()!=null) {
            if(getActivity() instanceof PaymentDetailActivity) {
                ((PaymentDetailActivity) getActivity()).payFlag = payFlag;
                ((PaymentDetailActivity) getActivity()).removeFragment("ezetap");
            }
            getActivity().onBackPressed();
        }
    }

}
