package com.skyinnolabs.consumerapp;

import android.content.SharedPreferences;
import android.provider.Settings;

public class SigncatchPref {

    public static void setAccessToken(String access_token) {
        SharedPreferences.Editor editor = SIGNCATCH.appSharedPreferences.edit();
        editor.putString("access_token", access_token);
        editor.commit();
    }

    public static String getAccessToken() {
        return SIGNCATCH.appSharedPreferences.getString("access_token", "");
    }

    public static void setStoreId(String id) {
        SharedPreferences.Editor editor = SIGNCATCH.appSharedPreferences.edit();
        editor.putString("storeId",id);
        editor.commit();
    }

    public static String getStoreId() {
        return SIGNCATCH.appSharedPreferences.getString("storeId","1");
    }

    public static void setBharatQrCredentials(String userId)
    {
        SharedPreferences.Editor editor = SIGNCATCH.appSharedPreferences.edit();
        editor.putString("bharat_qr_credentials",userId);
        editor.commit();
    }

    public static String getBharatQrCredentials()
    {
        return SIGNCATCH.appSharedPreferences.getString("bharat_qr_credentials","");
    }

    public static void setQrResponse(String userId)
    {
        SharedPreferences.Editor editor = SIGNCATCH.appSharedPreferences.edit();
        editor.putString("bharat_qr_response",userId);
        editor.commit();
    }

    public static String getQrResponse()
    {
        return SIGNCATCH.appSharedPreferences.getString("bharat_qr_response","");
    }

    public static void setQrPayment(String userId)
    {
        SharedPreferences.Editor editor = SIGNCATCH.appSharedPreferences.edit();
        editor.putString("qr_payment",userId);
        editor.commit();
    }

    public static String getQrPayment()
    {
        return SIGNCATCH.appSharedPreferences.getString("qr_payment","");
    }

    public static void setLoginType(int type)
    {
        SharedPreferences.Editor editor = SIGNCATCH.appSharedPreferences.edit();
        editor.putInt("loginType",type);
        editor.commit();
    }

    public static int getLoginType()
    {
        return SIGNCATCH.appSharedPreferences.getInt("loginType",0);
    }

    public static String getMerchantId() {
        return SIGNCATCH.appSharedPreferences.getString("id", "");
    }

    public static void setMerchantId(String id) {
        SharedPreferences.Editor editor = SIGNCATCH.appSharedPreferences.edit();
        editor.putString("id", id);
        editor.commit();
    }

    public static String getDeviceName(){
        return SIGNCATCH.appSharedPreferences.getString("deviceId", Settings.Secure.getString(SIGNCATCH.getContext().getContentResolver(), Settings.Secure.ANDROID_ID));
    }

}
