package com.skyinnolabs.consumerapp;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import io.socket.emitter.Emitter;

import static com.skyinnolabs.consumerapp.SigncatchPref.getDeviceName;

public class SplashActivity extends Activity implements WebSocketCommunicationLisntner{

    List<ImageView> arr_letter_O;
    Boolean isLoadingCompleted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        arr_letter_O = new ArrayList<ImageView>();
        arr_letter_O.add((ImageView) findViewById(R.id.logoImage1));
        arr_letter_O.add((ImageView) findViewById(R.id.logoImage2));
        arr_letter_O.add((ImageView) findViewById(R.id.logoImage3));
        arr_letter_O.add((ImageView) findViewById(R.id.logoImage4));

        rotate(90,arr_letter_O.get(0));

        SIGNCATCH.EZETAP = getDeviceName().contains("A920");

        if(SIGNCATCH.mSocket!=null && SIGNCATCH.mSocket.connected())
        {
            SIGNCATCH.registerDevice();
        }
        else if(SIGNCATCH.mSocket != null && !SIGNCATCH.mSocket.connected())
        {
            if (SIGNCATCH.mSocket == null || !SIGNCATCH.mSocket.connected()) {
                ((SIGNCATCH) SIGNCATCH.getContext()).getSocket();
            }
            if(SIGNCATCH.mSocket.connected()) {
                SIGNCATCH.registerDevice();
            }
        }
        ((SIGNCATCH)SIGNCATCH.getContext()).setWebSocketListner(this);
/*        SIGNCATCH.setDataToDeviceLinstener(dataToDeviceLinstener);*/
    }

    // Method for rotate animation
    private void rotate(float degree, final ImageView v) {
        v.setVisibility(View.VISIBLE);
        final RotateAnimation rotateAnim = new RotateAnimation(0.0f, degree,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f);
        rotateAnim.setDuration(700);
        rotateAnim.setFillAfter(false);
        v.startAnimation(rotateAnim);
        rotateAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                v.setVisibility(View.INVISIBLE);
                int index = arr_letter_O.indexOf(v);
                if(index<3)
                {
                    rotate(90,arr_letter_O.get(index+1));
                }
                else {
                    arr_letter_O.get(0).setVisibility(View.VISIBLE);
                    isLoadingCompleted =true;
/*                   String servertype ="0";
                   if(getString(R.string.basic_url).contains("signcatchkdp.com"))
                     servertype="1";*/
                    start();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
/*        SIGNCATCH.offDataToDeviceLinstener(dataToDeviceLinstener);*/
        ((SIGNCATCH)SIGNCATCH.getContext()).disconnectWebSocket();
    }

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(manufacturer) + " " + model;
    }

    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;

//        String phrase = "";
        StringBuilder phrase = new StringBuilder();
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
//                phrase += Character.toUpperCase(c);
                phrase.append(Character.toUpperCase(c));
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
//            phrase += c;
            phrase.append(c);
        }

        return phrase.toString();
    }

    private void start()
    {
        if(isLoadingCompleted) {
            if (!SigncatchPref.getAccessToken().equals("") && !SigncatchPref.getStoreId().equals("")) {
                startActivity(new Intent(SplashActivity.this, HomeActivity.class));
            } else
                startActivity(new Intent(SplashActivity.this, LoginActivity.class));
        }
    }

    @Override
    public void onConnected(Object... args) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                SIGNCATCH.registerDevice();
            }
        });
    }
    @Override
    public void onDisconnected(Object... args) {

    }

/*    public Emitter.Listener dataToDeviceLinstener = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String url = "";
                    Toast.makeText(SplashActivity.this,"New Order!",Toast.LENGTH_SHORT).show();
//                    Log.e("Permission websocket", args[0].toString());
                    //dialog = ProgressDialog.show(MainActivity.this,"","fetching permissions...");
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                }
            });
        }
    };*/

}
